// Compiled by ClojureScript 0.0-2030
goog.provide('async_tut1.core');
goog.require('cljs.core');
goog.require('cljs.core.async');
goog.require('goog.Uri');
goog.require('goog.net.Jsonp');
goog.require('cljs.core.async');
goog.require('goog.events');
goog.require('goog.events');
goog.require('goog.dom');
goog.require('goog.dom');
async_tut1.core.wiki_search_url = "http://en.wikipedia.org/w/api.php?action=opensearch&format=json&search=";
async_tut1.core.listen = (function listen(el,type){var out = cljs.core.async.chan.call(null);goog.events.listen(el,type,(function (e){return cljs.core.async.put_BANG_.call(null,out,e);
}));
return out;
});
async_tut1.core.jsonp = (function jsonp(uri){var out = cljs.core.async.chan.call(null);var req = (new goog.net.Jsonp((new goog.Uri(uri))));req.send(null,(function (res){return cljs.core.async.put_BANG_.call(null,out,res);
}));
return out;
});
async_tut1.core.query_url = (function query_url(q){return [cljs.core.str(async_tut1.core.wiki_search_url),cljs.core.str(q)].join('');
});
async_tut1.core.user_query = (function user_query(){return goog.dom.getElement("query").value;
});
async_tut1.core.render_query = (function render_query(results){return [cljs.core.str("<ul>"),cljs.core.str(cljs.core.apply.call(null,cljs.core.str,(function (){var iter__3817__auto__ = (function iter__17283(s__17284){return (new cljs.core.LazySeq(null,(function (){var s__17284__$1 = s__17284;while(true){
var temp__4092__auto__ = cljs.core.seq.call(null,s__17284__$1);if(temp__4092__auto__)
{var s__17284__$2 = temp__4092__auto__;if(cljs.core.chunked_seq_QMARK_.call(null,s__17284__$2))
{var c__3815__auto__ = cljs.core.chunk_first.call(null,s__17284__$2);var size__3816__auto__ = cljs.core.count.call(null,c__3815__auto__);var b__17286 = cljs.core.chunk_buffer.call(null,size__3816__auto__);if((function (){var i__17285 = 0;while(true){
if((i__17285 < size__3816__auto__))
{var result = cljs.core._nth.call(null,c__3815__auto__,i__17285);cljs.core.chunk_append.call(null,b__17286,[cljs.core.str("<li>"),cljs.core.str(result),cljs.core.str("</li>")].join(''));
{
var G__17287 = (i__17285 + 1);
i__17285 = G__17287;
continue;
}
} else
{return true;
}
break;
}
})())
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__17286),iter__17283.call(null,cljs.core.chunk_rest.call(null,s__17284__$2)));
} else
{return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__17286),null);
}
} else
{var result = cljs.core.first.call(null,s__17284__$2);return cljs.core.cons.call(null,[cljs.core.str("<li>"),cljs.core.str(result),cljs.core.str("</li>")].join(''),iter__17283.call(null,cljs.core.rest.call(null,s__17284__$2)));
}
} else
{return null;
}
break;
}
}),null,null));
});return iter__3817__auto__.call(null,results);
})())),cljs.core.str("</ul>")].join('');
});
async_tut1.core.init = (function init(){var clicks = async_tut1.core.listen.call(null,goog.dom.getElement("search"),"click");var results_view = goog.dom.getElement("results");var c__7221__auto__ = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_17347){var state_val_17348 = (state_17347[1]);if((state_val_17348 === 8))
{var inst_17335 = (state_17347[2]);var inst_17336 = cljs.core.nth.call(null,inst_17335,0,null);var inst_17337 = cljs.core.nth.call(null,inst_17335,1,null);var inst_17338 = async_tut1.core.render_query.call(null,inst_17337);var inst_17339 = results_view.innerHTML = inst_17338;var state_17347__$1 = (function (){var statearr_17349 = state_17347;(statearr_17349[7] = inst_17336);
(statearr_17349[8] = inst_17339);
return statearr_17349;
})();var statearr_17350_17364 = state_17347__$1;(statearr_17350_17364[2] = null);
(statearr_17350_17364[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_17348 === 7))
{var inst_17329 = (state_17347[2]);var inst_17331 = async_tut1.core.user_query.call(null);var inst_17332 = async_tut1.core.query_url.call(null,inst_17331);var inst_17333 = async_tut1.core.jsonp.call(null,inst_17332);var state_17347__$1 = (function (){var statearr_17351 = state_17347;(statearr_17351[9] = inst_17329);
return statearr_17351;
})();return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_17347__$1,8,inst_17333);
} else
{if((state_val_17348 === 6))
{var inst_17343 = (state_17347[2]);var state_17347__$1 = state_17347;var statearr_17352_17365 = state_17347__$1;(statearr_17352_17365[2] = inst_17343);
(statearr_17352_17365[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_17348 === 5))
{var state_17347__$1 = state_17347;var statearr_17353_17366 = state_17347__$1;(statearr_17353_17366[2] = null);
(statearr_17353_17366[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_17348 === 4))
{var state_17347__$1 = state_17347;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_17347__$1,7,clicks);
} else
{if((state_val_17348 === 3))
{var inst_17345 = (state_17347[2]);var state_17347__$1 = state_17347;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_17347__$1,inst_17345);
} else
{if((state_val_17348 === 2))
{var state_17347__$1 = state_17347;if(true)
{var statearr_17354_17367 = state_17347__$1;(statearr_17354_17367[1] = 4);
} else
{var statearr_17355_17368 = state_17347__$1;(statearr_17355_17368[1] = 5);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_17348 === 1))
{var state_17347__$1 = state_17347;var statearr_17356_17369 = state_17347__$1;(statearr_17356_17369[2] = null);
(statearr_17356_17369[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_17360 = (new Array(10));(statearr_17360[0] = state_machine__7152__auto__);
(statearr_17360[1] = 1);
return statearr_17360;
});
var state_machine__7152__auto____1 = (function (state_17347){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_17347);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e17361){if((e17361 instanceof Object))
{var ex__7155__auto__ = e17361;var statearr_17362_17370 = state_17347;(statearr_17362_17370[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_17347);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e17361;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__17371 = state_17347;
state_17347 = G__17371;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_17347){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_17347);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_17363 = f__7222__auto__.call(null);(statearr_17363[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto__);
return statearr_17363;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return c__7221__auto__;
});
async_tut1.core.init.call(null);

//# sourceMappingURL=core.js.map
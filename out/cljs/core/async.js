// Compiled by ClojureScript 0.0-2030
goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.ioc_helpers');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.ioc_helpers');
cljs.core.async.fn_handler = (function fn_handler(f){if(typeof cljs.core.async.t14070 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t14070 = (function (f,fn_handler,meta14071){
this.f = f;
this.fn_handler = fn_handler;
this.meta14071 = meta14071;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t14070.cljs$lang$type = true;
cljs.core.async.t14070.cljs$lang$ctorStr = "cljs.core.async/t14070";
cljs.core.async.t14070.cljs$lang$ctorPrWriter = (function (this__3668__auto__,writer__3669__auto__,opt__3670__auto__){return cljs.core._write.call(null,writer__3669__auto__,"cljs.core.async/t14070");
});
cljs.core.async.t14070.prototype.cljs$core$async$impl$protocols$Handler$ = true;
cljs.core.async.t14070.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return true;
});
cljs.core.async.t14070.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return self__.f;
});
cljs.core.async.t14070.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_14072){var self__ = this;
var _14072__$1 = this;return self__.meta14071;
});
cljs.core.async.t14070.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_14072,meta14071__$1){var self__ = this;
var _14072__$1 = this;return (new cljs.core.async.t14070(self__.f,self__.fn_handler,meta14071__$1));
});
cljs.core.async.__GT_t14070 = (function __GT_t14070(f__$1,fn_handler__$1,meta14071){return (new cljs.core.async.t14070(f__$1,fn_handler__$1,meta14071));
});
}
return (new cljs.core.async.t14070(f,fn_handler,null));
});
/**
* Returns a fixed buffer of size n. When full, puts will block/park.
*/
cljs.core.async.buffer = (function buffer(n){return cljs.core.async.impl.buffers.fixed_buffer.call(null,n);
});
/**
* Returns a buffer of size n. When full, puts will complete but
* val will be dropped (no transfer).
*/
cljs.core.async.dropping_buffer = (function dropping_buffer(n){return cljs.core.async.impl.buffers.dropping_buffer.call(null,n);
});
/**
* Returns a buffer of size n. When full, puts will complete, and be
* buffered, but oldest elements in buffer will be dropped (not
* transferred).
*/
cljs.core.async.sliding_buffer = (function sliding_buffer(n){return cljs.core.async.impl.buffers.sliding_buffer.call(null,n);
});
/**
* Returns true if a channel created with buff will never block. That is to say,
* puts into this buffer will never cause the buffer to be full.
*/
cljs.core.async.unblocking_buffer_QMARK_ = (function unblocking_buffer_QMARK_(buff){var G__14074 = buff;if(G__14074)
{var bit__3750__auto__ = null;if(cljs.core.truth_((function (){var or__3131__auto__ = bit__3750__auto__;if(cljs.core.truth_(or__3131__auto__))
{return or__3131__auto__;
} else
{return G__14074.cljs$core$async$impl$protocols$UnblockingBuffer$;
}
})()))
{return true;
} else
{if((!G__14074.cljs$lang$protocol_mask$partition$))
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,G__14074);
} else
{return false;
}
}
} else
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,G__14074);
}
});
/**
* Creates a channel with an optional buffer. If buf-or-n is a number,
* will create and use a fixed buffer of that size.
*/
cljs.core.async.chan = (function() {
var chan = null;
var chan__0 = (function (){return chan.call(null,null);
});
var chan__1 = (function (buf_or_n){var buf_or_n__$1 = ((cljs.core._EQ_.call(null,buf_or_n,0))?null:buf_or_n);return cljs.core.async.impl.channels.chan.call(null,((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer.call(null,buf_or_n__$1):buf_or_n__$1));
});
chan = function(buf_or_n){
switch(arguments.length){
case 0:
return chan__0.call(this);
case 1:
return chan__1.call(this,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
chan.cljs$core$IFn$_invoke$arity$0 = chan__0;
chan.cljs$core$IFn$_invoke$arity$1 = chan__1;
return chan;
})()
;
/**
* Returns a channel that will close after msecs
*/
cljs.core.async.timeout = (function timeout(msecs){return cljs.core.async.impl.timers.timeout.call(null,msecs);
});
/**
* takes a val from port. Must be called inside a (go ...) block. Will
* return nil if closed. Will park if nothing is available.
*/
cljs.core.async._LT__BANG_ = (function _LT__BANG_(port){if(null)
{return null;
} else
{throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str("<! used not in (go ...) block"),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,null))].join('')));
}
});
/**
* Asynchronously takes a val from port, passing to fn1. Will pass nil
* if closed. If on-caller? (default true) is true, and value is
* immediately available, will call fn1 on calling thread.
* Returns nil.
*/
cljs.core.async.take_BANG_ = (function() {
var take_BANG_ = null;
var take_BANG___2 = (function (port,fn1){return take_BANG_.call(null,port,fn1,true);
});
var take_BANG___3 = (function (port,fn1,on_caller_QMARK_){var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,fn1));if(cljs.core.truth_(ret))
{var val_14075 = cljs.core.deref.call(null,ret);if(cljs.core.truth_(on_caller_QMARK_))
{fn1.call(null,val_14075);
} else
{cljs.core.async.impl.dispatch.run.call(null,(function (){return fn1.call(null,val_14075);
}));
}
} else
{}
return null;
});
take_BANG_ = function(port,fn1,on_caller_QMARK_){
switch(arguments.length){
case 2:
return take_BANG___2.call(this,port,fn1);
case 3:
return take_BANG___3.call(this,port,fn1,on_caller_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
take_BANG_.cljs$core$IFn$_invoke$arity$2 = take_BANG___2;
take_BANG_.cljs$core$IFn$_invoke$arity$3 = take_BANG___3;
return take_BANG_;
})()
;
cljs.core.async.nop = (function nop(){return null;
});
/**
* puts a val into port. nil values are not allowed. Must be called
* inside a (go ...) block. Will park if no buffer space is available.
*/
cljs.core.async._GT__BANG_ = (function _GT__BANG_(port,val){if(null)
{return null;
} else
{throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str(">! used not in (go ...) block"),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,null))].join('')));
}
});
/**
* Asynchronously puts a val into port, calling fn0 (if supplied) when
* complete. nil values are not allowed. Will throw if closed. If
* on-caller? (default true) is true, and the put is immediately
* accepted, will call fn0 on calling thread.  Returns nil.
*/
cljs.core.async.put_BANG_ = (function() {
var put_BANG_ = null;
var put_BANG___2 = (function (port,val){return put_BANG_.call(null,port,val,cljs.core.async.nop);
});
var put_BANG___3 = (function (port,val,fn0){return put_BANG_.call(null,port,val,fn0,true);
});
var put_BANG___4 = (function (port,val,fn0,on_caller_QMARK_){var ret = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,fn0));if(cljs.core.truth_((function (){var and__3122__auto__ = ret;if(cljs.core.truth_(and__3122__auto__))
{return cljs.core.not_EQ_.call(null,fn0,cljs.core.async.nop);
} else
{return and__3122__auto__;
}
})()))
{if(cljs.core.truth_(on_caller_QMARK_))
{fn0.call(null);
} else
{cljs.core.async.impl.dispatch.run.call(null,fn0);
}
} else
{}
return null;
});
put_BANG_ = function(port,val,fn0,on_caller_QMARK_){
switch(arguments.length){
case 2:
return put_BANG___2.call(this,port,val);
case 3:
return put_BANG___3.call(this,port,val,fn0);
case 4:
return put_BANG___4.call(this,port,val,fn0,on_caller_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
put_BANG_.cljs$core$IFn$_invoke$arity$2 = put_BANG___2;
put_BANG_.cljs$core$IFn$_invoke$arity$3 = put_BANG___3;
put_BANG_.cljs$core$IFn$_invoke$arity$4 = put_BANG___4;
return put_BANG_;
})()
;
cljs.core.async.close_BANG_ = (function close_BANG_(port){return cljs.core.async.impl.protocols.close_BANG_.call(null,port);
});
cljs.core.async.random_array = (function random_array(n){var a = (new Array(n));var n__3920__auto___14076 = n;var x_14077 = 0;while(true){
if((x_14077 < n__3920__auto___14076))
{(a[x_14077] = 0);
{
var G__14078 = (x_14077 + 1);
x_14077 = G__14078;
continue;
}
} else
{}
break;
}
var i = 1;while(true){
if(cljs.core._EQ_.call(null,i,n))
{return a;
} else
{var j = cljs.core.rand_int.call(null,i);(a[i] = (a[j]));
(a[j] = i);
{
var G__14079 = (i + 1);
i = G__14079;
continue;
}
}
break;
}
});
cljs.core.async.alt_flag = (function alt_flag(){var flag = cljs.core.atom.call(null,true);if(typeof cljs.core.async.t14083 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t14083 = (function (flag,alt_flag,meta14084){
this.flag = flag;
this.alt_flag = alt_flag;
this.meta14084 = meta14084;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t14083.cljs$lang$type = true;
cljs.core.async.t14083.cljs$lang$ctorStr = "cljs.core.async/t14083";
cljs.core.async.t14083.cljs$lang$ctorPrWriter = (function (this__3668__auto__,writer__3669__auto__,opt__3670__auto__){return cljs.core._write.call(null,writer__3669__auto__,"cljs.core.async/t14083");
});
cljs.core.async.t14083.prototype.cljs$core$async$impl$protocols$Handler$ = true;
cljs.core.async.t14083.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return cljs.core.deref.call(null,self__.flag);
});
cljs.core.async.t14083.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){var self__ = this;
var ___$1 = this;cljs.core.reset_BANG_.call(null,self__.flag,null);
return true;
});
cljs.core.async.t14083.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_14085){var self__ = this;
var _14085__$1 = this;return self__.meta14084;
});
cljs.core.async.t14083.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_14085,meta14084__$1){var self__ = this;
var _14085__$1 = this;return (new cljs.core.async.t14083(self__.flag,self__.alt_flag,meta14084__$1));
});
cljs.core.async.__GT_t14083 = (function __GT_t14083(flag__$1,alt_flag__$1,meta14084){return (new cljs.core.async.t14083(flag__$1,alt_flag__$1,meta14084));
});
}
return (new cljs.core.async.t14083(flag,alt_flag,null));
});
cljs.core.async.alt_handler = (function alt_handler(flag,cb){if(typeof cljs.core.async.t14089 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t14089 = (function (cb,flag,alt_handler,meta14090){
this.cb = cb;
this.flag = flag;
this.alt_handler = alt_handler;
this.meta14090 = meta14090;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t14089.cljs$lang$type = true;
cljs.core.async.t14089.cljs$lang$ctorStr = "cljs.core.async/t14089";
cljs.core.async.t14089.cljs$lang$ctorPrWriter = (function (this__3668__auto__,writer__3669__auto__,opt__3670__auto__){return cljs.core._write.call(null,writer__3669__auto__,"cljs.core.async/t14089");
});
cljs.core.async.t14089.prototype.cljs$core$async$impl$protocols$Handler$ = true;
cljs.core.async.t14089.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.flag);
});
cljs.core.async.t14089.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){var self__ = this;
var ___$1 = this;cljs.core.async.impl.protocols.commit.call(null,self__.flag);
return self__.cb;
});
cljs.core.async.t14089.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_14091){var self__ = this;
var _14091__$1 = this;return self__.meta14090;
});
cljs.core.async.t14089.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_14091,meta14090__$1){var self__ = this;
var _14091__$1 = this;return (new cljs.core.async.t14089(self__.cb,self__.flag,self__.alt_handler,meta14090__$1));
});
cljs.core.async.__GT_t14089 = (function __GT_t14089(cb__$1,flag__$1,alt_handler__$1,meta14090){return (new cljs.core.async.t14089(cb__$1,flag__$1,alt_handler__$1,meta14090));
});
}
return (new cljs.core.async.t14089(cb,flag,alt_handler,null));
});
/**
* returns derefable [val port] if immediate, nil if enqueued
*/
cljs.core.async.do_alts = (function do_alts(fret,ports,opts){var flag = cljs.core.async.alt_flag.call(null);var n = cljs.core.count.call(null,ports);var idxs = cljs.core.async.random_array.call(null,n);var priority = new cljs.core.Keyword(null,"priority","priority",4143410454).cljs$core$IFn$_invoke$arity$1(opts);var ret = (function (){var i = 0;while(true){
if((i < n))
{var idx = (cljs.core.truth_(priority)?i:(idxs[i]));var port = cljs.core.nth.call(null,ports,idx);var wport = ((cljs.core.vector_QMARK_.call(null,port))?port.call(null,0):null);var vbox = (cljs.core.truth_(wport)?(function (){var val = port.call(null,1);return cljs.core.async.impl.protocols.put_BANG_.call(null,wport,val,cljs.core.async.alt_handler.call(null,flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (){return fret.call(null,cljs.core.PersistentVector.fromArray([null,wport], true));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.alt_handler.call(null,flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__14092_SHARP_){return fret.call(null,cljs.core.PersistentVector.fromArray([p1__14092_SHARP_,port], true));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));if(cljs.core.truth_(vbox))
{return cljs.core.async.impl.channels.box.call(null,cljs.core.PersistentVector.fromArray([cljs.core.deref.call(null,vbox),(function (){var or__3131__auto__ = wport;if(cljs.core.truth_(or__3131__auto__))
{return or__3131__auto__;
} else
{return port;
}
})()], true));
} else
{{
var G__14093 = (i + 1);
i = G__14093;
continue;
}
}
} else
{return null;
}
break;
}
})();var or__3131__auto__ = ret;if(cljs.core.truth_(or__3131__auto__))
{return or__3131__auto__;
} else
{if(cljs.core.contains_QMARK_.call(null,opts,new cljs.core.Keyword(null,"default","default",2558708147)))
{var temp__4092__auto__ = (function (){var and__3122__auto__ = cljs.core.async.impl.protocols.active_QMARK_.call(null,flag);if(cljs.core.truth_(and__3122__auto__))
{return cljs.core.async.impl.protocols.commit.call(null,flag);
} else
{return and__3122__auto__;
}
})();if(cljs.core.truth_(temp__4092__auto__))
{var got = temp__4092__auto__;return cljs.core.async.impl.channels.box.call(null,cljs.core.PersistentVector.fromArray([new cljs.core.Keyword(null,"default","default",2558708147).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",2558708147)], true));
} else
{return null;
}
} else
{return null;
}
}
});
/**
* Completes at most one of several channel operations. Must be called
* inside a (go ...) block. ports is a vector of channel endpoints, which
* can be either a channel to take from or a vector of
* [channel-to-put-to val-to-put], in any combination. Takes will be
* made as if by <!, and puts will be made as if by >!. Unless
* the :priority option is true, if more than one port operation is
* ready a non-deterministic choice will be made. If no operation is
* ready and a :default value is supplied, [default-val :default] will
* be returned, otherwise alts! will park until the first operation to
* become ready completes. Returns [val port] of the completed
* operation, where val is the value taken for takes, and nil for puts.
* 
* opts are passed as :key val ... Supported options:
* 
* :default val - the value to use if none of the operations are immediately ready
* :priority true - (default nil) when true, the operations will be tried in order.
* 
* Note: there is no guarantee that the port exps or val exprs will be
* used, nor in what order should they be, so they should not be
* depended upon for side effects.
* @param {...*} var_args
*/
cljs.core.async.alts_BANG_ = (function() { 
var alts_BANG___delegate = function (ports,p__14094){var map__14096 = p__14094;var map__14096__$1 = ((cljs.core.seq_QMARK_.call(null,map__14096))?cljs.core.apply.call(null,cljs.core.hash_map,map__14096):map__14096);var opts = map__14096__$1;if(null)
{return null;
} else
{throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str("alts! used not in (go ...) block"),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,null))].join('')));
}
};
var alts_BANG_ = function (ports,var_args){
var p__14094 = null;if (arguments.length > 1) {
  p__14094 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return alts_BANG___delegate.call(this,ports,p__14094);};
alts_BANG_.cljs$lang$maxFixedArity = 1;
alts_BANG_.cljs$lang$applyTo = (function (arglist__14097){
var ports = cljs.core.first(arglist__14097);
var p__14094 = cljs.core.rest(arglist__14097);
return alts_BANG___delegate(ports,p__14094);
});
alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = alts_BANG___delegate;
return alts_BANG_;
})()
;
/**
* Takes a function and a source channel, and returns a channel which
* contains the values produced by applying f to each value taken from
* the source channel
*/
cljs.core.async.map_LT_ = (function map_LT_(f,ch){if(typeof cljs.core.async.t14105 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t14105 = (function (ch,f,map_LT_,meta14106){
this.ch = ch;
this.f = f;
this.map_LT_ = map_LT_;
this.meta14106 = meta14106;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t14105.cljs$lang$type = true;
cljs.core.async.t14105.cljs$lang$ctorStr = "cljs.core.async/t14105";
cljs.core.async.t14105.cljs$lang$ctorPrWriter = (function (this__3668__auto__,writer__3669__auto__,opt__3670__auto__){return cljs.core._write.call(null,writer__3669__auto__,"cljs.core.async/t14105");
});
cljs.core.async.t14105.prototype.cljs$core$async$impl$protocols$WritePort$ = true;
cljs.core.async.t14105.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn0){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn0);
});
cljs.core.async.t14105.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;
cljs.core.async.t14105.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){var self__ = this;
var ___$1 = this;var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,(function (){if(typeof cljs.core.async.t14108 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t14108 = (function (fn1,_,meta14106,ch,f,map_LT_,meta14109){
this.fn1 = fn1;
this._ = _;
this.meta14106 = meta14106;
this.ch = ch;
this.f = f;
this.map_LT_ = map_LT_;
this.meta14109 = meta14109;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t14108.cljs$lang$type = true;
cljs.core.async.t14108.cljs$lang$ctorStr = "cljs.core.async/t14108";
cljs.core.async.t14108.cljs$lang$ctorPrWriter = (function (this__3668__auto__,writer__3669__auto__,opt__3670__auto__){return cljs.core._write.call(null,writer__3669__auto__,"cljs.core.async/t14108");
});
cljs.core.async.t14108.prototype.cljs$core$async$impl$protocols$Handler$ = true;
cljs.core.async.t14108.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (___$3){var self__ = this;
var ___$4 = this;return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.fn1);
});
cljs.core.async.t14108.prototype.cljs$core$async$impl$protocols$Handler$lock_id$arity$1 = (function (___$3){var self__ = this;
var ___$4 = this;return cljs.core.async.impl.protocols.lock_id.call(null,self__.fn1);
});
cljs.core.async.t14108.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (___$3){var self__ = this;
var ___$4 = this;var f1 = cljs.core.async.impl.protocols.commit.call(null,self__.fn1);return ((function (f1,___$4){
return (function (p1__14098_SHARP_){return f1.call(null,(((p1__14098_SHARP_ == null))?null:self__.f.call(null,p1__14098_SHARP_)));
});
;})(f1,___$4))
});
cljs.core.async.t14108.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_14110){var self__ = this;
var _14110__$1 = this;return self__.meta14109;
});
cljs.core.async.t14108.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_14110,meta14109__$1){var self__ = this;
var _14110__$1 = this;return (new cljs.core.async.t14108(self__.fn1,self__._,self__.meta14106,self__.ch,self__.f,self__.map_LT_,meta14109__$1));
});
cljs.core.async.__GT_t14108 = (function __GT_t14108(fn1__$1,___$2,meta14106__$1,ch__$2,f__$2,map_LT___$2,meta14109){return (new cljs.core.async.t14108(fn1__$1,___$2,meta14106__$1,ch__$2,f__$2,map_LT___$2,meta14109));
});
}
return (new cljs.core.async.t14108(fn1,___$1,self__.meta14106,self__.ch,self__.f,self__.map_LT_,null));
})());if(cljs.core.truth_((function (){var and__3122__auto__ = ret;if(cljs.core.truth_(and__3122__auto__))
{return !((cljs.core.deref.call(null,ret) == null));
} else
{return and__3122__auto__;
}
})()))
{return cljs.core.async.impl.channels.box.call(null,self__.f.call(null,cljs.core.deref.call(null,ret)));
} else
{return ret;
}
});
cljs.core.async.t14105.prototype.cljs$core$async$impl$protocols$Channel$ = true;
cljs.core.async.t14105.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});
cljs.core.async.t14105.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_14107){var self__ = this;
var _14107__$1 = this;return self__.meta14106;
});
cljs.core.async.t14105.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_14107,meta14106__$1){var self__ = this;
var _14107__$1 = this;return (new cljs.core.async.t14105(self__.ch,self__.f,self__.map_LT_,meta14106__$1));
});
cljs.core.async.__GT_t14105 = (function __GT_t14105(ch__$1,f__$1,map_LT___$1,meta14106){return (new cljs.core.async.t14105(ch__$1,f__$1,map_LT___$1,meta14106));
});
}
return (new cljs.core.async.t14105(ch,f,map_LT_,null));
});
/**
* Takes a function and a target channel, and returns a channel which
* applies f to each value before supplying it to the target channel.
*/
cljs.core.async.map_GT_ = (function map_GT_(f,ch){if(typeof cljs.core.async.t14114 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t14114 = (function (ch,f,map_GT_,meta14115){
this.ch = ch;
this.f = f;
this.map_GT_ = map_GT_;
this.meta14115 = meta14115;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t14114.cljs$lang$type = true;
cljs.core.async.t14114.cljs$lang$ctorStr = "cljs.core.async/t14114";
cljs.core.async.t14114.cljs$lang$ctorPrWriter = (function (this__3668__auto__,writer__3669__auto__,opt__3670__auto__){return cljs.core._write.call(null,writer__3669__auto__,"cljs.core.async/t14114");
});
cljs.core.async.t14114.prototype.cljs$core$async$impl$protocols$WritePort$ = true;
cljs.core.async.t14114.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn0){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,self__.f.call(null,val),fn0);
});
cljs.core.async.t14114.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;
cljs.core.async.t14114.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});
cljs.core.async.t14114.prototype.cljs$core$async$impl$protocols$Channel$ = true;
cljs.core.async.t14114.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});
cljs.core.async.t14114.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_14116){var self__ = this;
var _14116__$1 = this;return self__.meta14115;
});
cljs.core.async.t14114.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_14116,meta14115__$1){var self__ = this;
var _14116__$1 = this;return (new cljs.core.async.t14114(self__.ch,self__.f,self__.map_GT_,meta14115__$1));
});
cljs.core.async.__GT_t14114 = (function __GT_t14114(ch__$1,f__$1,map_GT___$1,meta14115){return (new cljs.core.async.t14114(ch__$1,f__$1,map_GT___$1,meta14115));
});
}
return (new cljs.core.async.t14114(ch,f,map_GT_,null));
});
/**
* Takes a predicate and a target channel, and returns a channel which
* supplies only the values for which the predicate returns true to the
* target channel.
*/
cljs.core.async.filter_GT_ = (function filter_GT_(p,ch){if(typeof cljs.core.async.t14120 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t14120 = (function (ch,p,filter_GT_,meta14121){
this.ch = ch;
this.p = p;
this.filter_GT_ = filter_GT_;
this.meta14121 = meta14121;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t14120.cljs$lang$type = true;
cljs.core.async.t14120.cljs$lang$ctorStr = "cljs.core.async/t14120";
cljs.core.async.t14120.cljs$lang$ctorPrWriter = (function (this__3668__auto__,writer__3669__auto__,opt__3670__auto__){return cljs.core._write.call(null,writer__3669__auto__,"cljs.core.async/t14120");
});
cljs.core.async.t14120.prototype.cljs$core$async$impl$protocols$WritePort$ = true;
cljs.core.async.t14120.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn0){var self__ = this;
var ___$1 = this;if(cljs.core.truth_(self__.p.call(null,val)))
{return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn0);
} else
{return cljs.core.async.impl.channels.box.call(null,null);
}
});
cljs.core.async.t14120.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;
cljs.core.async.t14120.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});
cljs.core.async.t14120.prototype.cljs$core$async$impl$protocols$Channel$ = true;
cljs.core.async.t14120.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});
cljs.core.async.t14120.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_14122){var self__ = this;
var _14122__$1 = this;return self__.meta14121;
});
cljs.core.async.t14120.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_14122,meta14121__$1){var self__ = this;
var _14122__$1 = this;return (new cljs.core.async.t14120(self__.ch,self__.p,self__.filter_GT_,meta14121__$1));
});
cljs.core.async.__GT_t14120 = (function __GT_t14120(ch__$1,p__$1,filter_GT___$1,meta14121){return (new cljs.core.async.t14120(ch__$1,p__$1,filter_GT___$1,meta14121));
});
}
return (new cljs.core.async.t14120(ch,p,filter_GT_,null));
});
/**
* Takes a predicate and a target channel, and returns a channel which
* supplies only the values for which the predicate returns false to the
* target channel.
*/
cljs.core.async.remove_GT_ = (function remove_GT_(p,ch){return cljs.core.async.filter_GT_.call(null,cljs.core.complement.call(null,p),ch);
});
/**
* Takes a predicate and a source channel, and returns a channel which
* contains only the values taken from the source channel for which the
* predicate returns true. The returned channel will be unbuffered by
* default, or a buf-or-n can be supplied. The channel will close
* when the source channel closes.
*/
cljs.core.async.filter_LT_ = (function() {
var filter_LT_ = null;
var filter_LT___2 = (function (p,ch){return filter_LT_.call(null,p,ch,null);
});
var filter_LT___3 = (function (p,ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__7221__auto___14205 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_14184){var state_val_14185 = (state_14184[1]);if((state_val_14185 === 1))
{var state_14184__$1 = state_14184;var statearr_14186_14206 = state_14184__$1;(statearr_14186_14206[2] = null);
(statearr_14186_14206[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14185 === 2))
{var state_14184__$1 = state_14184;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_14184__$1,4,ch);
} else
{if((state_val_14185 === 3))
{var inst_14182 = (state_14184[2]);var state_14184__$1 = state_14184;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_14184__$1,inst_14182);
} else
{if((state_val_14185 === 4))
{var inst_14166 = (state_14184[7]);var inst_14166__$1 = (state_14184[2]);var inst_14167 = (inst_14166__$1 == null);var state_14184__$1 = (function (){var statearr_14187 = state_14184;(statearr_14187[7] = inst_14166__$1);
return statearr_14187;
})();if(cljs.core.truth_(inst_14167))
{var statearr_14188_14207 = state_14184__$1;(statearr_14188_14207[1] = 5);
} else
{var statearr_14189_14208 = state_14184__$1;(statearr_14189_14208[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14185 === 5))
{var inst_14169 = cljs.core.async.close_BANG_.call(null,out);var state_14184__$1 = state_14184;var statearr_14190_14209 = state_14184__$1;(statearr_14190_14209[2] = inst_14169);
(statearr_14190_14209[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14185 === 6))
{var inst_14166 = (state_14184[7]);var inst_14171 = p.call(null,inst_14166);var state_14184__$1 = state_14184;if(cljs.core.truth_(inst_14171))
{var statearr_14191_14210 = state_14184__$1;(statearr_14191_14210[1] = 8);
} else
{var statearr_14192_14211 = state_14184__$1;(statearr_14192_14211[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14185 === 7))
{var inst_14180 = (state_14184[2]);var state_14184__$1 = state_14184;var statearr_14193_14212 = state_14184__$1;(statearr_14193_14212[2] = inst_14180);
(statearr_14193_14212[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14185 === 8))
{var inst_14166 = (state_14184[7]);var state_14184__$1 = state_14184;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_14184__$1,11,out,inst_14166);
} else
{if((state_val_14185 === 9))
{var state_14184__$1 = state_14184;var statearr_14194_14213 = state_14184__$1;(statearr_14194_14213[2] = null);
(statearr_14194_14213[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14185 === 10))
{var inst_14177 = (state_14184[2]);var state_14184__$1 = (function (){var statearr_14195 = state_14184;(statearr_14195[8] = inst_14177);
return statearr_14195;
})();var statearr_14196_14214 = state_14184__$1;(statearr_14196_14214[2] = null);
(statearr_14196_14214[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14185 === 11))
{var inst_14174 = (state_14184[2]);var state_14184__$1 = state_14184;var statearr_14197_14215 = state_14184__$1;(statearr_14197_14215[2] = inst_14174);
(statearr_14197_14215[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_14201 = (new Array(9));(statearr_14201[0] = state_machine__7152__auto__);
(statearr_14201[1] = 1);
return statearr_14201;
});
var state_machine__7152__auto____1 = (function (state_14184){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_14184);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e14202){if((e14202 instanceof Object))
{var ex__7155__auto__ = e14202;var statearr_14203_14216 = state_14184;(statearr_14203_14216[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_14184);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e14202;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__14217 = state_14184;
state_14184 = G__14217;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_14184){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_14184);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_14204 = f__7222__auto__.call(null);(statearr_14204[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto___14205);
return statearr_14204;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return out;
});
filter_LT_ = function(p,ch,buf_or_n){
switch(arguments.length){
case 2:
return filter_LT___2.call(this,p,ch);
case 3:
return filter_LT___3.call(this,p,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
filter_LT_.cljs$core$IFn$_invoke$arity$2 = filter_LT___2;
filter_LT_.cljs$core$IFn$_invoke$arity$3 = filter_LT___3;
return filter_LT_;
})()
;
/**
* Takes a predicate and a source channel, and returns a channel which
* contains only the values taken from the source channel for which the
* predicate returns false. The returned channel will be unbuffered by
* default, or a buf-or-n can be supplied. The channel will close
* when the source channel closes.
*/
cljs.core.async.remove_LT_ = (function() {
var remove_LT_ = null;
var remove_LT___2 = (function (p,ch){return remove_LT_.call(null,p,ch,null);
});
var remove_LT___3 = (function (p,ch,buf_or_n){return cljs.core.async.filter_LT_.call(null,cljs.core.complement.call(null,p),ch,buf_or_n);
});
remove_LT_ = function(p,ch,buf_or_n){
switch(arguments.length){
case 2:
return remove_LT___2.call(this,p,ch);
case 3:
return remove_LT___3.call(this,p,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
remove_LT_.cljs$core$IFn$_invoke$arity$2 = remove_LT___2;
remove_LT_.cljs$core$IFn$_invoke$arity$3 = remove_LT___3;
return remove_LT_;
})()
;
cljs.core.async.mapcat_STAR_ = (function mapcat_STAR_(f,in$,out){var c__7221__auto__ = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_14369){var state_val_14370 = (state_14369[1]);if((state_val_14370 === 1))
{var state_14369__$1 = state_14369;var statearr_14371_14408 = state_14369__$1;(statearr_14371_14408[2] = null);
(statearr_14371_14408[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 2))
{var state_14369__$1 = state_14369;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_14369__$1,4,in$);
} else
{if((state_val_14370 === 3))
{var inst_14367 = (state_14369[2]);var state_14369__$1 = state_14369;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_14369__$1,inst_14367);
} else
{if((state_val_14370 === 4))
{var inst_14315 = (state_14369[7]);var inst_14315__$1 = (state_14369[2]);var inst_14316 = (inst_14315__$1 == null);var state_14369__$1 = (function (){var statearr_14372 = state_14369;(statearr_14372[7] = inst_14315__$1);
return statearr_14372;
})();if(cljs.core.truth_(inst_14316))
{var statearr_14373_14409 = state_14369__$1;(statearr_14373_14409[1] = 5);
} else
{var statearr_14374_14410 = state_14369__$1;(statearr_14374_14410[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 5))
{var inst_14318 = cljs.core.async.close_BANG_.call(null,out);var state_14369__$1 = state_14369;var statearr_14375_14411 = state_14369__$1;(statearr_14375_14411[2] = inst_14318);
(statearr_14375_14411[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 6))
{var inst_14315 = (state_14369[7]);var inst_14320 = f.call(null,inst_14315);var inst_14325 = cljs.core.seq.call(null,inst_14320);var inst_14326 = inst_14325;var inst_14327 = null;var inst_14328 = 0;var inst_14329 = 0;var state_14369__$1 = (function (){var statearr_14376 = state_14369;(statearr_14376[8] = inst_14327);
(statearr_14376[9] = inst_14328);
(statearr_14376[10] = inst_14329);
(statearr_14376[11] = inst_14326);
return statearr_14376;
})();var statearr_14377_14412 = state_14369__$1;(statearr_14377_14412[2] = null);
(statearr_14377_14412[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 7))
{var inst_14365 = (state_14369[2]);var state_14369__$1 = state_14369;var statearr_14378_14413 = state_14369__$1;(statearr_14378_14413[2] = inst_14365);
(statearr_14378_14413[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 8))
{var inst_14328 = (state_14369[9]);var inst_14329 = (state_14369[10]);var inst_14331 = (inst_14329 < inst_14328);var inst_14332 = inst_14331;var state_14369__$1 = state_14369;if(cljs.core.truth_(inst_14332))
{var statearr_14379_14414 = state_14369__$1;(statearr_14379_14414[1] = 10);
} else
{var statearr_14380_14415 = state_14369__$1;(statearr_14380_14415[1] = 11);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 9))
{var inst_14362 = (state_14369[2]);var state_14369__$1 = (function (){var statearr_14381 = state_14369;(statearr_14381[12] = inst_14362);
return statearr_14381;
})();var statearr_14382_14416 = state_14369__$1;(statearr_14382_14416[2] = null);
(statearr_14382_14416[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 10))
{var inst_14327 = (state_14369[8]);var inst_14329 = (state_14369[10]);var inst_14334 = cljs.core._nth.call(null,inst_14327,inst_14329);var state_14369__$1 = state_14369;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_14369__$1,13,out,inst_14334);
} else
{if((state_val_14370 === 11))
{var inst_14326 = (state_14369[11]);var inst_14340 = (state_14369[13]);var inst_14340__$1 = cljs.core.seq.call(null,inst_14326);var state_14369__$1 = (function (){var statearr_14386 = state_14369;(statearr_14386[13] = inst_14340__$1);
return statearr_14386;
})();if(inst_14340__$1)
{var statearr_14387_14417 = state_14369__$1;(statearr_14387_14417[1] = 14);
} else
{var statearr_14388_14418 = state_14369__$1;(statearr_14388_14418[1] = 15);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 12))
{var inst_14360 = (state_14369[2]);var state_14369__$1 = state_14369;var statearr_14389_14419 = state_14369__$1;(statearr_14389_14419[2] = inst_14360);
(statearr_14389_14419[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 13))
{var inst_14327 = (state_14369[8]);var inst_14328 = (state_14369[9]);var inst_14329 = (state_14369[10]);var inst_14326 = (state_14369[11]);var inst_14336 = (state_14369[2]);var inst_14337 = (inst_14329 + 1);var tmp14383 = inst_14327;var tmp14384 = inst_14328;var tmp14385 = inst_14326;var inst_14326__$1 = tmp14385;var inst_14327__$1 = tmp14383;var inst_14328__$1 = tmp14384;var inst_14329__$1 = inst_14337;var state_14369__$1 = (function (){var statearr_14390 = state_14369;(statearr_14390[8] = inst_14327__$1);
(statearr_14390[14] = inst_14336);
(statearr_14390[9] = inst_14328__$1);
(statearr_14390[10] = inst_14329__$1);
(statearr_14390[11] = inst_14326__$1);
return statearr_14390;
})();var statearr_14391_14420 = state_14369__$1;(statearr_14391_14420[2] = null);
(statearr_14391_14420[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 14))
{var inst_14340 = (state_14369[13]);var inst_14342 = cljs.core.chunked_seq_QMARK_.call(null,inst_14340);var state_14369__$1 = state_14369;if(inst_14342)
{var statearr_14392_14421 = state_14369__$1;(statearr_14392_14421[1] = 17);
} else
{var statearr_14393_14422 = state_14369__$1;(statearr_14393_14422[1] = 18);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 15))
{var state_14369__$1 = state_14369;var statearr_14394_14423 = state_14369__$1;(statearr_14394_14423[2] = null);
(statearr_14394_14423[1] = 16);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 16))
{var inst_14358 = (state_14369[2]);var state_14369__$1 = state_14369;var statearr_14395_14424 = state_14369__$1;(statearr_14395_14424[2] = inst_14358);
(statearr_14395_14424[1] = 12);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 17))
{var inst_14340 = (state_14369[13]);var inst_14344 = cljs.core.chunk_first.call(null,inst_14340);var inst_14345 = cljs.core.chunk_rest.call(null,inst_14340);var inst_14346 = cljs.core.count.call(null,inst_14344);var inst_14326 = inst_14345;var inst_14327 = inst_14344;var inst_14328 = inst_14346;var inst_14329 = 0;var state_14369__$1 = (function (){var statearr_14396 = state_14369;(statearr_14396[8] = inst_14327);
(statearr_14396[9] = inst_14328);
(statearr_14396[10] = inst_14329);
(statearr_14396[11] = inst_14326);
return statearr_14396;
})();var statearr_14397_14425 = state_14369__$1;(statearr_14397_14425[2] = null);
(statearr_14397_14425[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 18))
{var inst_14340 = (state_14369[13]);var inst_14349 = cljs.core.first.call(null,inst_14340);var state_14369__$1 = state_14369;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_14369__$1,20,out,inst_14349);
} else
{if((state_val_14370 === 19))
{var inst_14355 = (state_14369[2]);var state_14369__$1 = state_14369;var statearr_14398_14426 = state_14369__$1;(statearr_14398_14426[2] = inst_14355);
(statearr_14398_14426[1] = 16);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14370 === 20))
{var inst_14340 = (state_14369[13]);var inst_14351 = (state_14369[2]);var inst_14352 = cljs.core.next.call(null,inst_14340);var inst_14326 = inst_14352;var inst_14327 = null;var inst_14328 = 0;var inst_14329 = 0;var state_14369__$1 = (function (){var statearr_14399 = state_14369;(statearr_14399[8] = inst_14327);
(statearr_14399[9] = inst_14328);
(statearr_14399[10] = inst_14329);
(statearr_14399[11] = inst_14326);
(statearr_14399[15] = inst_14351);
return statearr_14399;
})();var statearr_14400_14427 = state_14369__$1;(statearr_14400_14427[2] = null);
(statearr_14400_14427[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_14404 = (new Array(16));(statearr_14404[0] = state_machine__7152__auto__);
(statearr_14404[1] = 1);
return statearr_14404;
});
var state_machine__7152__auto____1 = (function (state_14369){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_14369);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e14405){if((e14405 instanceof Object))
{var ex__7155__auto__ = e14405;var statearr_14406_14428 = state_14369;(statearr_14406_14428[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_14369);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e14405;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__14429 = state_14369;
state_14369 = G__14429;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_14369){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_14369);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_14407 = f__7222__auto__.call(null);(statearr_14407[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto__);
return statearr_14407;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return c__7221__auto__;
});
/**
* Takes a function and a source channel, and returns a channel which
* contains the values in each collection produced by applying f to
* each value taken from the source channel. f must return a
* collection.
* 
* The returned channel will be unbuffered by default, or a buf-or-n
* can be supplied. The channel will close when the source channel
* closes.
*/
cljs.core.async.mapcat_LT_ = (function() {
var mapcat_LT_ = null;
var mapcat_LT___2 = (function (f,in$){return mapcat_LT_.call(null,f,in$,null);
});
var mapcat_LT___3 = (function (f,in$,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);cljs.core.async.mapcat_STAR_.call(null,f,in$,out);
return out;
});
mapcat_LT_ = function(f,in$,buf_or_n){
switch(arguments.length){
case 2:
return mapcat_LT___2.call(this,f,in$);
case 3:
return mapcat_LT___3.call(this,f,in$,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = mapcat_LT___2;
mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = mapcat_LT___3;
return mapcat_LT_;
})()
;
/**
* Takes a function and a target channel, and returns a channel which
* applies f to each value put, then supplies each element of the result
* to the target channel. f must return a collection.
* 
* The returned channel will be unbuffered by default, or a buf-or-n
* can be supplied. The target channel will be closed when the source
* channel closes.
*/
cljs.core.async.mapcat_GT_ = (function() {
var mapcat_GT_ = null;
var mapcat_GT___2 = (function (f,out){return mapcat_GT_.call(null,f,out,null);
});
var mapcat_GT___3 = (function (f,out,buf_or_n){var in$ = cljs.core.async.chan.call(null,buf_or_n);cljs.core.async.mapcat_STAR_.call(null,f,in$,out);
return in$;
});
mapcat_GT_ = function(f,out,buf_or_n){
switch(arguments.length){
case 2:
return mapcat_GT___2.call(this,f,out);
case 3:
return mapcat_GT___3.call(this,f,out,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = mapcat_GT___2;
mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = mapcat_GT___3;
return mapcat_GT_;
})()
;
/**
* Takes elements from the from channel and supplies them to the to
* channel. By default, the to channel will be closed when the
* from channel closes, but can be determined by the close?
* parameter.
*/
cljs.core.async.pipe = (function() {
var pipe = null;
var pipe__2 = (function (from,to){return pipe.call(null,from,to,true);
});
var pipe__3 = (function (from,to,close_QMARK_){var c__7221__auto___14510 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_14489){var state_val_14490 = (state_14489[1]);if((state_val_14490 === 1))
{var state_14489__$1 = state_14489;var statearr_14491_14511 = state_14489__$1;(statearr_14491_14511[2] = null);
(statearr_14491_14511[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14490 === 2))
{var state_14489__$1 = state_14489;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_14489__$1,4,from);
} else
{if((state_val_14490 === 3))
{var inst_14487 = (state_14489[2]);var state_14489__$1 = state_14489;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_14489__$1,inst_14487);
} else
{if((state_val_14490 === 4))
{var inst_14472 = (state_14489[7]);var inst_14472__$1 = (state_14489[2]);var inst_14473 = (inst_14472__$1 == null);var state_14489__$1 = (function (){var statearr_14492 = state_14489;(statearr_14492[7] = inst_14472__$1);
return statearr_14492;
})();if(cljs.core.truth_(inst_14473))
{var statearr_14493_14512 = state_14489__$1;(statearr_14493_14512[1] = 5);
} else
{var statearr_14494_14513 = state_14489__$1;(statearr_14494_14513[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14490 === 5))
{var state_14489__$1 = state_14489;if(cljs.core.truth_(close_QMARK_))
{var statearr_14495_14514 = state_14489__$1;(statearr_14495_14514[1] = 8);
} else
{var statearr_14496_14515 = state_14489__$1;(statearr_14496_14515[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14490 === 6))
{var inst_14472 = (state_14489[7]);var state_14489__$1 = state_14489;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_14489__$1,11,to,inst_14472);
} else
{if((state_val_14490 === 7))
{var inst_14485 = (state_14489[2]);var state_14489__$1 = state_14489;var statearr_14497_14516 = state_14489__$1;(statearr_14497_14516[2] = inst_14485);
(statearr_14497_14516[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14490 === 8))
{var inst_14476 = cljs.core.async.close_BANG_.call(null,to);var state_14489__$1 = state_14489;var statearr_14498_14517 = state_14489__$1;(statearr_14498_14517[2] = inst_14476);
(statearr_14498_14517[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14490 === 9))
{var state_14489__$1 = state_14489;var statearr_14499_14518 = state_14489__$1;(statearr_14499_14518[2] = null);
(statearr_14499_14518[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14490 === 10))
{var inst_14479 = (state_14489[2]);var state_14489__$1 = state_14489;var statearr_14500_14519 = state_14489__$1;(statearr_14500_14519[2] = inst_14479);
(statearr_14500_14519[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14490 === 11))
{var inst_14482 = (state_14489[2]);var state_14489__$1 = (function (){var statearr_14501 = state_14489;(statearr_14501[8] = inst_14482);
return statearr_14501;
})();var statearr_14502_14520 = state_14489__$1;(statearr_14502_14520[2] = null);
(statearr_14502_14520[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_14506 = (new Array(9));(statearr_14506[0] = state_machine__7152__auto__);
(statearr_14506[1] = 1);
return statearr_14506;
});
var state_machine__7152__auto____1 = (function (state_14489){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_14489);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e14507){if((e14507 instanceof Object))
{var ex__7155__auto__ = e14507;var statearr_14508_14521 = state_14489;(statearr_14508_14521[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_14489);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e14507;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__14522 = state_14489;
state_14489 = G__14522;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_14489){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_14489);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_14509 = f__7222__auto__.call(null);(statearr_14509[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto___14510);
return statearr_14509;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return to;
});
pipe = function(from,to,close_QMARK_){
switch(arguments.length){
case 2:
return pipe__2.call(this,from,to);
case 3:
return pipe__3.call(this,from,to,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
pipe.cljs$core$IFn$_invoke$arity$2 = pipe__2;
pipe.cljs$core$IFn$_invoke$arity$3 = pipe__3;
return pipe;
})()
;
/**
* Takes a predicate and a source channel and returns a vector of two
* channels, the first of which will contain the values for which the
* predicate returned true, the second those for which it returned
* false.
* 
* The out channels will be unbuffered by default, or two buf-or-ns can
* be supplied. The channels will close after the source channel has
* closed.
*/
cljs.core.async.split = (function() {
var split = null;
var split__2 = (function (p,ch){return split.call(null,p,ch,null,null);
});
var split__4 = (function (p,ch,t_buf_or_n,f_buf_or_n){var tc = cljs.core.async.chan.call(null,t_buf_or_n);var fc = cljs.core.async.chan.call(null,f_buf_or_n);var c__7221__auto___14609 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_14587){var state_val_14588 = (state_14587[1]);if((state_val_14588 === 1))
{var state_14587__$1 = state_14587;var statearr_14589_14610 = state_14587__$1;(statearr_14589_14610[2] = null);
(statearr_14589_14610[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14588 === 2))
{var state_14587__$1 = state_14587;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_14587__$1,4,ch);
} else
{if((state_val_14588 === 3))
{var inst_14585 = (state_14587[2]);var state_14587__$1 = state_14587;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_14587__$1,inst_14585);
} else
{if((state_val_14588 === 4))
{var inst_14568 = (state_14587[7]);var inst_14568__$1 = (state_14587[2]);var inst_14569 = (inst_14568__$1 == null);var state_14587__$1 = (function (){var statearr_14590 = state_14587;(statearr_14590[7] = inst_14568__$1);
return statearr_14590;
})();if(cljs.core.truth_(inst_14569))
{var statearr_14591_14611 = state_14587__$1;(statearr_14591_14611[1] = 5);
} else
{var statearr_14592_14612 = state_14587__$1;(statearr_14592_14612[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14588 === 5))
{var inst_14571 = cljs.core.async.close_BANG_.call(null,tc);var inst_14572 = cljs.core.async.close_BANG_.call(null,fc);var state_14587__$1 = (function (){var statearr_14593 = state_14587;(statearr_14593[8] = inst_14571);
return statearr_14593;
})();var statearr_14594_14613 = state_14587__$1;(statearr_14594_14613[2] = inst_14572);
(statearr_14594_14613[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14588 === 6))
{var inst_14568 = (state_14587[7]);var inst_14574 = p.call(null,inst_14568);var state_14587__$1 = state_14587;if(cljs.core.truth_(inst_14574))
{var statearr_14595_14614 = state_14587__$1;(statearr_14595_14614[1] = 9);
} else
{var statearr_14596_14615 = state_14587__$1;(statearr_14596_14615[1] = 10);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14588 === 7))
{var inst_14583 = (state_14587[2]);var state_14587__$1 = state_14587;var statearr_14597_14616 = state_14587__$1;(statearr_14597_14616[2] = inst_14583);
(statearr_14597_14616[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14588 === 8))
{var inst_14580 = (state_14587[2]);var state_14587__$1 = (function (){var statearr_14598 = state_14587;(statearr_14598[9] = inst_14580);
return statearr_14598;
})();var statearr_14599_14617 = state_14587__$1;(statearr_14599_14617[2] = null);
(statearr_14599_14617[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14588 === 9))
{var state_14587__$1 = state_14587;var statearr_14600_14618 = state_14587__$1;(statearr_14600_14618[2] = tc);
(statearr_14600_14618[1] = 11);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14588 === 10))
{var state_14587__$1 = state_14587;var statearr_14601_14619 = state_14587__$1;(statearr_14601_14619[2] = fc);
(statearr_14601_14619[1] = 11);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14588 === 11))
{var inst_14568 = (state_14587[7]);var inst_14578 = (state_14587[2]);var state_14587__$1 = state_14587;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_14587__$1,8,inst_14578,inst_14568);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_14605 = (new Array(10));(statearr_14605[0] = state_machine__7152__auto__);
(statearr_14605[1] = 1);
return statearr_14605;
});
var state_machine__7152__auto____1 = (function (state_14587){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_14587);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e14606){if((e14606 instanceof Object))
{var ex__7155__auto__ = e14606;var statearr_14607_14620 = state_14587;(statearr_14607_14620[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_14587);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e14606;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__14621 = state_14587;
state_14587 = G__14621;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_14587){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_14587);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_14608 = f__7222__auto__.call(null);(statearr_14608[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto___14609);
return statearr_14608;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return cljs.core.PersistentVector.fromArray([tc,fc], true);
});
split = function(p,ch,t_buf_or_n,f_buf_or_n){
switch(arguments.length){
case 2:
return split__2.call(this,p,ch);
case 4:
return split__4.call(this,p,ch,t_buf_or_n,f_buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
split.cljs$core$IFn$_invoke$arity$2 = split__2;
split.cljs$core$IFn$_invoke$arity$4 = split__4;
return split;
})()
;
/**
* f should be a function of 2 arguments. Returns a channel containing
* the single result of applying f to init and the first item from the
* channel, then applying f to that result and the 2nd item, etc. If
* the channel closes without yielding items, returns init and f is not
* called. ch must close before reduce produces a result.
*/
cljs.core.async.reduce = (function reduce(f,init,ch){var c__7221__auto__ = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_14668){var state_val_14669 = (state_14668[1]);if((state_val_14669 === 7))
{var inst_14664 = (state_14668[2]);var state_14668__$1 = state_14668;var statearr_14670_14686 = state_14668__$1;(statearr_14670_14686[2] = inst_14664);
(statearr_14670_14686[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14669 === 6))
{var inst_14654 = (state_14668[7]);var inst_14657 = (state_14668[8]);var inst_14661 = f.call(null,inst_14654,inst_14657);var inst_14654__$1 = inst_14661;var state_14668__$1 = (function (){var statearr_14671 = state_14668;(statearr_14671[7] = inst_14654__$1);
return statearr_14671;
})();var statearr_14672_14687 = state_14668__$1;(statearr_14672_14687[2] = null);
(statearr_14672_14687[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14669 === 5))
{var inst_14654 = (state_14668[7]);var state_14668__$1 = state_14668;var statearr_14673_14688 = state_14668__$1;(statearr_14673_14688[2] = inst_14654);
(statearr_14673_14688[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14669 === 4))
{var inst_14657 = (state_14668[8]);var inst_14657__$1 = (state_14668[2]);var inst_14658 = (inst_14657__$1 == null);var state_14668__$1 = (function (){var statearr_14674 = state_14668;(statearr_14674[8] = inst_14657__$1);
return statearr_14674;
})();if(cljs.core.truth_(inst_14658))
{var statearr_14675_14689 = state_14668__$1;(statearr_14675_14689[1] = 5);
} else
{var statearr_14676_14690 = state_14668__$1;(statearr_14676_14690[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14669 === 3))
{var inst_14666 = (state_14668[2]);var state_14668__$1 = state_14668;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_14668__$1,inst_14666);
} else
{if((state_val_14669 === 2))
{var state_14668__$1 = state_14668;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_14668__$1,4,ch);
} else
{if((state_val_14669 === 1))
{var inst_14654 = init;var state_14668__$1 = (function (){var statearr_14677 = state_14668;(statearr_14677[7] = inst_14654);
return statearr_14677;
})();var statearr_14678_14691 = state_14668__$1;(statearr_14678_14691[2] = null);
(statearr_14678_14691[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_14682 = (new Array(9));(statearr_14682[0] = state_machine__7152__auto__);
(statearr_14682[1] = 1);
return statearr_14682;
});
var state_machine__7152__auto____1 = (function (state_14668){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_14668);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e14683){if((e14683 instanceof Object))
{var ex__7155__auto__ = e14683;var statearr_14684_14692 = state_14668;(statearr_14684_14692[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_14668);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e14683;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__14693 = state_14668;
state_14668 = G__14693;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_14668){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_14668);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_14685 = f__7222__auto__.call(null);(statearr_14685[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto__);
return statearr_14685;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return c__7221__auto__;
});
/**
* Puts the contents of coll into the supplied channel.
* 
* By default the channel will be closed after the items are copied,
* but can be determined by the close? parameter.
* 
* Returns a channel which will close after the items are copied.
*/
cljs.core.async.onto_chan = (function() {
var onto_chan = null;
var onto_chan__2 = (function (ch,coll){return onto_chan.call(null,ch,coll,true);
});
var onto_chan__3 = (function (ch,coll,close_QMARK_){var c__7221__auto__ = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_14755){var state_val_14756 = (state_14755[1]);if((state_val_14756 === 1))
{var inst_14735 = cljs.core.seq.call(null,coll);var inst_14736 = inst_14735;var state_14755__$1 = (function (){var statearr_14757 = state_14755;(statearr_14757[7] = inst_14736);
return statearr_14757;
})();var statearr_14758_14776 = state_14755__$1;(statearr_14758_14776[2] = null);
(statearr_14758_14776[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14756 === 2))
{var inst_14736 = (state_14755[7]);var state_14755__$1 = state_14755;if(cljs.core.truth_(inst_14736))
{var statearr_14759_14777 = state_14755__$1;(statearr_14759_14777[1] = 4);
} else
{var statearr_14760_14778 = state_14755__$1;(statearr_14760_14778[1] = 5);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14756 === 3))
{var inst_14753 = (state_14755[2]);var state_14755__$1 = state_14755;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_14755__$1,inst_14753);
} else
{if((state_val_14756 === 4))
{var inst_14736 = (state_14755[7]);var inst_14739 = cljs.core.first.call(null,inst_14736);var state_14755__$1 = state_14755;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_14755__$1,7,ch,inst_14739);
} else
{if((state_val_14756 === 5))
{var state_14755__$1 = state_14755;if(cljs.core.truth_(close_QMARK_))
{var statearr_14761_14779 = state_14755__$1;(statearr_14761_14779[1] = 8);
} else
{var statearr_14762_14780 = state_14755__$1;(statearr_14762_14780[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14756 === 6))
{var inst_14751 = (state_14755[2]);var state_14755__$1 = state_14755;var statearr_14763_14781 = state_14755__$1;(statearr_14763_14781[2] = inst_14751);
(statearr_14763_14781[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14756 === 7))
{var inst_14736 = (state_14755[7]);var inst_14741 = (state_14755[2]);var inst_14742 = cljs.core.next.call(null,inst_14736);var inst_14736__$1 = inst_14742;var state_14755__$1 = (function (){var statearr_14764 = state_14755;(statearr_14764[7] = inst_14736__$1);
(statearr_14764[8] = inst_14741);
return statearr_14764;
})();var statearr_14765_14782 = state_14755__$1;(statearr_14765_14782[2] = null);
(statearr_14765_14782[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14756 === 8))
{var inst_14746 = cljs.core.async.close_BANG_.call(null,ch);var state_14755__$1 = state_14755;var statearr_14766_14783 = state_14755__$1;(statearr_14766_14783[2] = inst_14746);
(statearr_14766_14783[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14756 === 9))
{var state_14755__$1 = state_14755;var statearr_14767_14784 = state_14755__$1;(statearr_14767_14784[2] = null);
(statearr_14767_14784[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_14756 === 10))
{var inst_14749 = (state_14755[2]);var state_14755__$1 = state_14755;var statearr_14768_14785 = state_14755__$1;(statearr_14768_14785[2] = inst_14749);
(statearr_14768_14785[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_14772 = (new Array(9));(statearr_14772[0] = state_machine__7152__auto__);
(statearr_14772[1] = 1);
return statearr_14772;
});
var state_machine__7152__auto____1 = (function (state_14755){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_14755);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e14773){if((e14773 instanceof Object))
{var ex__7155__auto__ = e14773;var statearr_14774_14786 = state_14755;(statearr_14774_14786[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_14755);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e14773;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__14787 = state_14755;
state_14755 = G__14787;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_14755){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_14755);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_14775 = f__7222__auto__.call(null);(statearr_14775[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto__);
return statearr_14775;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return c__7221__auto__;
});
onto_chan = function(ch,coll,close_QMARK_){
switch(arguments.length){
case 2:
return onto_chan__2.call(this,ch,coll);
case 3:
return onto_chan__3.call(this,ch,coll,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
onto_chan.cljs$core$IFn$_invoke$arity$2 = onto_chan__2;
onto_chan.cljs$core$IFn$_invoke$arity$3 = onto_chan__3;
return onto_chan;
})()
;
/**
* Creates and returns a channel which contains the contents of coll,
* closing when exhausted.
*/
cljs.core.async.to_chan = (function to_chan(coll){var ch = cljs.core.async.chan.call(null,cljs.core.bounded_count.call(null,100,coll));cljs.core.async.onto_chan.call(null,ch,coll);
return ch;
});
cljs.core.async.Mux = {};
cljs.core.async.muxch_STAR_ = (function muxch_STAR_(_){if((function (){var and__3122__auto__ = _;if(and__3122__auto__)
{return _.cljs$core$async$Mux$muxch_STAR_$arity$1;
} else
{return and__3122__auto__;
}
})())
{return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else
{var x__3727__auto__ = (((_ == null))?null:_);return (function (){var or__3131__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__3727__auto__)]);if(or__3131__auto__)
{return or__3131__auto__;
} else
{var or__3131__auto____$1 = (cljs.core.async.muxch_STAR_["_"]);if(or__3131__auto____$1)
{return or__3131__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mux.muxch*",_);
}
}
})().call(null,_);
}
});
cljs.core.async.Mult = {};
cljs.core.async.tap_STAR_ = (function tap_STAR_(m,ch,close_QMARK_){if((function (){var and__3122__auto__ = m;if(and__3122__auto__)
{return m.cljs$core$async$Mult$tap_STAR_$arity$3;
} else
{return and__3122__auto__;
}
})())
{return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else
{var x__3727__auto__ = (((m == null))?null:m);return (function (){var or__3131__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__3727__auto__)]);if(or__3131__auto__)
{return or__3131__auto__;
} else
{var or__3131__auto____$1 = (cljs.core.async.tap_STAR_["_"]);if(or__3131__auto____$1)
{return or__3131__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mult.tap*",m);
}
}
})().call(null,m,ch,close_QMARK_);
}
});
cljs.core.async.untap_STAR_ = (function untap_STAR_(m,ch){if((function (){var and__3122__auto__ = m;if(and__3122__auto__)
{return m.cljs$core$async$Mult$untap_STAR_$arity$2;
} else
{return and__3122__auto__;
}
})())
{return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else
{var x__3727__auto__ = (((m == null))?null:m);return (function (){var or__3131__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__3727__auto__)]);if(or__3131__auto__)
{return or__3131__auto__;
} else
{var or__3131__auto____$1 = (cljs.core.async.untap_STAR_["_"]);if(or__3131__auto____$1)
{return or__3131__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mult.untap*",m);
}
}
})().call(null,m,ch);
}
});
cljs.core.async.untap_all_STAR_ = (function untap_all_STAR_(m){if((function (){var and__3122__auto__ = m;if(and__3122__auto__)
{return m.cljs$core$async$Mult$untap_all_STAR_$arity$1;
} else
{return and__3122__auto__;
}
})())
{return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else
{var x__3727__auto__ = (((m == null))?null:m);return (function (){var or__3131__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__3727__auto__)]);if(or__3131__auto__)
{return or__3131__auto__;
} else
{var or__3131__auto____$1 = (cljs.core.async.untap_all_STAR_["_"]);if(or__3131__auto____$1)
{return or__3131__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mult.untap-all*",m);
}
}
})().call(null,m);
}
});
/**
* Creates and returns a mult(iple) of the supplied channel. Channels
* containing copies of the channel can be created with 'tap', and
* detached with 'untap'.
* 
* Each item is distributed to all taps in parallel and synchronously,
* i.e. each tap must accept before the next item is distributed. Use
* buffering/windowing to prevent slow taps from holding up the mult.
* 
* If a tap put throws an exception, it will be removed from the mult.
*/
cljs.core.async.mult = (function mult(ch){var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);var m = (function (){if(typeof cljs.core.async.t15002 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t15002 = (function (cs,ch,mult,meta15003){
this.cs = cs;
this.ch = ch;
this.mult = mult;
this.meta15003 = meta15003;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t15002.cljs$lang$type = true;
cljs.core.async.t15002.cljs$lang$ctorStr = "cljs.core.async/t15002";
cljs.core.async.t15002.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__3668__auto__,writer__3669__auto__,opt__3670__auto__){return cljs.core._write.call(null,writer__3669__auto__,"cljs.core.async/t15002");
});})(cs))
;
cljs.core.async.t15002.prototype.cljs$core$async$Mult$ = true;
cljs.core.async.t15002.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$2,close_QMARK_){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch__$2,close_QMARK_);
return null;
});})(cs))
;
cljs.core.async.t15002.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$2){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch__$2);
return null;
});})(cs))
;
cljs.core.async.t15002.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){var self__ = this;
var ___$1 = this;cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);
return null;
});})(cs))
;
cljs.core.async.t15002.prototype.cljs$core$async$Mux$ = true;
cljs.core.async.t15002.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){var self__ = this;
var ___$1 = this;return self__.ch;
});})(cs))
;
cljs.core.async.t15002.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_15004){var self__ = this;
var _15004__$1 = this;return self__.meta15003;
});})(cs))
;
cljs.core.async.t15002.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_15004,meta15003__$1){var self__ = this;
var _15004__$1 = this;return (new cljs.core.async.t15002(self__.cs,self__.ch,self__.mult,meta15003__$1));
});})(cs))
;
cljs.core.async.__GT_t15002 = ((function (cs){
return (function __GT_t15002(cs__$1,ch__$1,mult__$1,meta15003){return (new cljs.core.async.t15002(cs__$1,ch__$1,mult__$1,meta15003));
});})(cs))
;
}
return (new cljs.core.async.t15002(cs,ch,mult,null));
})();var dchan = cljs.core.async.chan.call(null,1);var dctr = cljs.core.atom.call(null,null);var done = ((function (cs,m,dchan,dctr){
return (function (){if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === 0))
{return cljs.core.async.put_BANG_.call(null,dchan,true);
} else
{return null;
}
});})(cs,m,dchan,dctr))
;var c__7221__auto___15216 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_15134){var state_val_15135 = (state_15134[1]);if((state_val_15135 === 32))
{var inst_15083 = (state_15134[7]);var inst_15007 = (state_15134[8]);var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_15134,31,Object,null,30);var inst_15090 = cljs.core.async.put_BANG_.call(null,inst_15083,inst_15007,done);var state_15134__$1 = state_15134;var statearr_15136_15217 = state_15134__$1;(statearr_15136_15217[2] = inst_15090);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_15134__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 1))
{var state_15134__$1 = state_15134;var statearr_15137_15218 = state_15134__$1;(statearr_15137_15218[2] = null);
(statearr_15137_15218[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 33))
{var inst_15096 = (state_15134[9]);var inst_15098 = cljs.core.chunked_seq_QMARK_.call(null,inst_15096);var state_15134__$1 = state_15134;if(inst_15098)
{var statearr_15138_15219 = state_15134__$1;(statearr_15138_15219[1] = 36);
} else
{var statearr_15139_15220 = state_15134__$1;(statearr_15139_15220[1] = 37);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 2))
{var state_15134__$1 = state_15134;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_15134__$1,4,ch);
} else
{if((state_val_15135 === 34))
{var state_15134__$1 = state_15134;var statearr_15140_15221 = state_15134__$1;(statearr_15140_15221[2] = null);
(statearr_15140_15221[1] = 35);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 3))
{var inst_15132 = (state_15134[2]);var state_15134__$1 = state_15134;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_15134__$1,inst_15132);
} else
{if((state_val_15135 === 35))
{var inst_15121 = (state_15134[2]);var state_15134__$1 = state_15134;var statearr_15141_15222 = state_15134__$1;(statearr_15141_15222[2] = inst_15121);
(statearr_15141_15222[1] = 29);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 4))
{var inst_15007 = (state_15134[8]);var inst_15007__$1 = (state_15134[2]);var inst_15008 = (inst_15007__$1 == null);var state_15134__$1 = (function (){var statearr_15142 = state_15134;(statearr_15142[8] = inst_15007__$1);
return statearr_15142;
})();if(cljs.core.truth_(inst_15008))
{var statearr_15143_15223 = state_15134__$1;(statearr_15143_15223[1] = 5);
} else
{var statearr_15144_15224 = state_15134__$1;(statearr_15144_15224[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 36))
{var inst_15096 = (state_15134[9]);var inst_15100 = cljs.core.chunk_first.call(null,inst_15096);var inst_15101 = cljs.core.chunk_rest.call(null,inst_15096);var inst_15102 = cljs.core.count.call(null,inst_15100);var inst_15075 = inst_15101;var inst_15076 = inst_15100;var inst_15077 = inst_15102;var inst_15078 = 0;var state_15134__$1 = (function (){var statearr_15145 = state_15134;(statearr_15145[10] = inst_15078);
(statearr_15145[11] = inst_15075);
(statearr_15145[12] = inst_15077);
(statearr_15145[13] = inst_15076);
return statearr_15145;
})();var statearr_15146_15225 = state_15134__$1;(statearr_15146_15225[2] = null);
(statearr_15146_15225[1] = 25);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 5))
{var inst_15014 = cljs.core.deref.call(null,cs);var inst_15015 = cljs.core.seq.call(null,inst_15014);var inst_15016 = inst_15015;var inst_15017 = null;var inst_15018 = 0;var inst_15019 = 0;var state_15134__$1 = (function (){var statearr_15147 = state_15134;(statearr_15147[14] = inst_15019);
(statearr_15147[15] = inst_15018);
(statearr_15147[16] = inst_15017);
(statearr_15147[17] = inst_15016);
return statearr_15147;
})();var statearr_15148_15226 = state_15134__$1;(statearr_15148_15226[2] = null);
(statearr_15148_15226[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 37))
{var inst_15096 = (state_15134[9]);var inst_15105 = cljs.core.first.call(null,inst_15096);var state_15134__$1 = (function (){var statearr_15149 = state_15134;(statearr_15149[18] = inst_15105);
return statearr_15149;
})();var statearr_15150_15227 = state_15134__$1;(statearr_15150_15227[2] = null);
(statearr_15150_15227[1] = 41);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 6))
{var inst_15066 = cljs.core.deref.call(null,cs);var inst_15067 = cljs.core.keys.call(null,inst_15066);var inst_15068 = cljs.core.count.call(null,inst_15067);var inst_15069 = cljs.core.reset_BANG_.call(null,dctr,inst_15068);var inst_15074 = cljs.core.seq.call(null,inst_15067);var inst_15075 = inst_15074;var inst_15076 = null;var inst_15077 = 0;var inst_15078 = 0;var state_15134__$1 = (function (){var statearr_15151 = state_15134;(statearr_15151[19] = inst_15069);
(statearr_15151[10] = inst_15078);
(statearr_15151[11] = inst_15075);
(statearr_15151[12] = inst_15077);
(statearr_15151[13] = inst_15076);
return statearr_15151;
})();var statearr_15152_15228 = state_15134__$1;(statearr_15152_15228[2] = null);
(statearr_15152_15228[1] = 25);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 38))
{var inst_15118 = (state_15134[2]);var state_15134__$1 = state_15134;var statearr_15153_15229 = state_15134__$1;(statearr_15153_15229[2] = inst_15118);
(statearr_15153_15229[1] = 35);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 7))
{var inst_15130 = (state_15134[2]);var state_15134__$1 = state_15134;var statearr_15154_15230 = state_15134__$1;(statearr_15154_15230[2] = inst_15130);
(statearr_15154_15230[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 39))
{var inst_15096 = (state_15134[9]);var inst_15114 = (state_15134[2]);var inst_15115 = cljs.core.next.call(null,inst_15096);var inst_15075 = inst_15115;var inst_15076 = null;var inst_15077 = 0;var inst_15078 = 0;var state_15134__$1 = (function (){var statearr_15155 = state_15134;(statearr_15155[20] = inst_15114);
(statearr_15155[10] = inst_15078);
(statearr_15155[11] = inst_15075);
(statearr_15155[12] = inst_15077);
(statearr_15155[13] = inst_15076);
return statearr_15155;
})();var statearr_15156_15231 = state_15134__$1;(statearr_15156_15231[2] = null);
(statearr_15156_15231[1] = 25);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 8))
{var inst_15019 = (state_15134[14]);var inst_15018 = (state_15134[15]);var inst_15021 = (inst_15019 < inst_15018);var inst_15022 = inst_15021;var state_15134__$1 = state_15134;if(cljs.core.truth_(inst_15022))
{var statearr_15157_15232 = state_15134__$1;(statearr_15157_15232[1] = 10);
} else
{var statearr_15158_15233 = state_15134__$1;(statearr_15158_15233[1] = 11);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 40))
{var inst_15105 = (state_15134[18]);var inst_15106 = (state_15134[2]);var inst_15107 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);var inst_15108 = cljs.core.async.untap_STAR_.call(null,m,inst_15105);var state_15134__$1 = (function (){var statearr_15159 = state_15134;(statearr_15159[21] = inst_15107);
(statearr_15159[22] = inst_15106);
return statearr_15159;
})();var statearr_15160_15234 = state_15134__$1;(statearr_15160_15234[2] = inst_15108);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_15134__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 9))
{var inst_15064 = (state_15134[2]);var state_15134__$1 = state_15134;var statearr_15161_15235 = state_15134__$1;(statearr_15161_15235[2] = inst_15064);
(statearr_15161_15235[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 41))
{var inst_15105 = (state_15134[18]);var inst_15007 = (state_15134[8]);var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_15134,40,Object,null,39);var inst_15112 = cljs.core.async.put_BANG_.call(null,inst_15105,inst_15007,done);var state_15134__$1 = state_15134;var statearr_15162_15236 = state_15134__$1;(statearr_15162_15236[2] = inst_15112);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_15134__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 10))
{var inst_15019 = (state_15134[14]);var inst_15017 = (state_15134[16]);var inst_15025 = cljs.core._nth.call(null,inst_15017,inst_15019);var inst_15026 = cljs.core.nth.call(null,inst_15025,0,null);var inst_15027 = cljs.core.nth.call(null,inst_15025,1,null);var state_15134__$1 = (function (){var statearr_15163 = state_15134;(statearr_15163[23] = inst_15026);
return statearr_15163;
})();if(cljs.core.truth_(inst_15027))
{var statearr_15164_15237 = state_15134__$1;(statearr_15164_15237[1] = 13);
} else
{var statearr_15165_15238 = state_15134__$1;(statearr_15165_15238[1] = 14);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 42))
{var inst_15127 = (state_15134[2]);var state_15134__$1 = (function (){var statearr_15166 = state_15134;(statearr_15166[24] = inst_15127);
return statearr_15166;
})();var statearr_15167_15239 = state_15134__$1;(statearr_15167_15239[2] = null);
(statearr_15167_15239[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 11))
{var inst_15016 = (state_15134[17]);var inst_15036 = (state_15134[25]);var inst_15036__$1 = cljs.core.seq.call(null,inst_15016);var state_15134__$1 = (function (){var statearr_15168 = state_15134;(statearr_15168[25] = inst_15036__$1);
return statearr_15168;
})();if(inst_15036__$1)
{var statearr_15169_15240 = state_15134__$1;(statearr_15169_15240[1] = 16);
} else
{var statearr_15170_15241 = state_15134__$1;(statearr_15170_15241[1] = 17);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 12))
{var inst_15062 = (state_15134[2]);var state_15134__$1 = state_15134;var statearr_15171_15242 = state_15134__$1;(statearr_15171_15242[2] = inst_15062);
(statearr_15171_15242[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 13))
{var inst_15026 = (state_15134[23]);var inst_15029 = cljs.core.async.close_BANG_.call(null,inst_15026);var state_15134__$1 = state_15134;var statearr_15175_15243 = state_15134__$1;(statearr_15175_15243[2] = inst_15029);
(statearr_15175_15243[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 14))
{var state_15134__$1 = state_15134;var statearr_15176_15244 = state_15134__$1;(statearr_15176_15244[2] = null);
(statearr_15176_15244[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 15))
{var inst_15019 = (state_15134[14]);var inst_15018 = (state_15134[15]);var inst_15017 = (state_15134[16]);var inst_15016 = (state_15134[17]);var inst_15032 = (state_15134[2]);var inst_15033 = (inst_15019 + 1);var tmp15172 = inst_15018;var tmp15173 = inst_15017;var tmp15174 = inst_15016;var inst_15016__$1 = tmp15174;var inst_15017__$1 = tmp15173;var inst_15018__$1 = tmp15172;var inst_15019__$1 = inst_15033;var state_15134__$1 = (function (){var statearr_15177 = state_15134;(statearr_15177[14] = inst_15019__$1);
(statearr_15177[15] = inst_15018__$1);
(statearr_15177[16] = inst_15017__$1);
(statearr_15177[17] = inst_15016__$1);
(statearr_15177[26] = inst_15032);
return statearr_15177;
})();var statearr_15178_15245 = state_15134__$1;(statearr_15178_15245[2] = null);
(statearr_15178_15245[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 16))
{var inst_15036 = (state_15134[25]);var inst_15038 = cljs.core.chunked_seq_QMARK_.call(null,inst_15036);var state_15134__$1 = state_15134;if(inst_15038)
{var statearr_15179_15246 = state_15134__$1;(statearr_15179_15246[1] = 19);
} else
{var statearr_15180_15247 = state_15134__$1;(statearr_15180_15247[1] = 20);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 17))
{var state_15134__$1 = state_15134;var statearr_15181_15248 = state_15134__$1;(statearr_15181_15248[2] = null);
(statearr_15181_15248[1] = 18);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 18))
{var inst_15060 = (state_15134[2]);var state_15134__$1 = state_15134;var statearr_15182_15249 = state_15134__$1;(statearr_15182_15249[2] = inst_15060);
(statearr_15182_15249[1] = 12);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 19))
{var inst_15036 = (state_15134[25]);var inst_15040 = cljs.core.chunk_first.call(null,inst_15036);var inst_15041 = cljs.core.chunk_rest.call(null,inst_15036);var inst_15042 = cljs.core.count.call(null,inst_15040);var inst_15016 = inst_15041;var inst_15017 = inst_15040;var inst_15018 = inst_15042;var inst_15019 = 0;var state_15134__$1 = (function (){var statearr_15183 = state_15134;(statearr_15183[14] = inst_15019);
(statearr_15183[15] = inst_15018);
(statearr_15183[16] = inst_15017);
(statearr_15183[17] = inst_15016);
return statearr_15183;
})();var statearr_15184_15250 = state_15134__$1;(statearr_15184_15250[2] = null);
(statearr_15184_15250[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 20))
{var inst_15036 = (state_15134[25]);var inst_15046 = cljs.core.first.call(null,inst_15036);var inst_15047 = cljs.core.nth.call(null,inst_15046,0,null);var inst_15048 = cljs.core.nth.call(null,inst_15046,1,null);var state_15134__$1 = (function (){var statearr_15185 = state_15134;(statearr_15185[27] = inst_15047);
return statearr_15185;
})();if(cljs.core.truth_(inst_15048))
{var statearr_15186_15251 = state_15134__$1;(statearr_15186_15251[1] = 22);
} else
{var statearr_15187_15252 = state_15134__$1;(statearr_15187_15252[1] = 23);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 21))
{var inst_15057 = (state_15134[2]);var state_15134__$1 = state_15134;var statearr_15188_15253 = state_15134__$1;(statearr_15188_15253[2] = inst_15057);
(statearr_15188_15253[1] = 18);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 22))
{var inst_15047 = (state_15134[27]);var inst_15050 = cljs.core.async.close_BANG_.call(null,inst_15047);var state_15134__$1 = state_15134;var statearr_15189_15254 = state_15134__$1;(statearr_15189_15254[2] = inst_15050);
(statearr_15189_15254[1] = 24);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 23))
{var state_15134__$1 = state_15134;var statearr_15190_15255 = state_15134__$1;(statearr_15190_15255[2] = null);
(statearr_15190_15255[1] = 24);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 24))
{var inst_15036 = (state_15134[25]);var inst_15053 = (state_15134[2]);var inst_15054 = cljs.core.next.call(null,inst_15036);var inst_15016 = inst_15054;var inst_15017 = null;var inst_15018 = 0;var inst_15019 = 0;var state_15134__$1 = (function (){var statearr_15191 = state_15134;(statearr_15191[28] = inst_15053);
(statearr_15191[14] = inst_15019);
(statearr_15191[15] = inst_15018);
(statearr_15191[16] = inst_15017);
(statearr_15191[17] = inst_15016);
return statearr_15191;
})();var statearr_15192_15256 = state_15134__$1;(statearr_15192_15256[2] = null);
(statearr_15192_15256[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 25))
{var inst_15078 = (state_15134[10]);var inst_15077 = (state_15134[12]);var inst_15080 = (inst_15078 < inst_15077);var inst_15081 = inst_15080;var state_15134__$1 = state_15134;if(cljs.core.truth_(inst_15081))
{var statearr_15193_15257 = state_15134__$1;(statearr_15193_15257[1] = 27);
} else
{var statearr_15194_15258 = state_15134__$1;(statearr_15194_15258[1] = 28);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 26))
{var inst_15125 = (state_15134[2]);var state_15134__$1 = (function (){var statearr_15195 = state_15134;(statearr_15195[29] = inst_15125);
return statearr_15195;
})();return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_15134__$1,42,dchan);
} else
{if((state_val_15135 === 27))
{var inst_15078 = (state_15134[10]);var inst_15076 = (state_15134[13]);var inst_15083 = cljs.core._nth.call(null,inst_15076,inst_15078);var state_15134__$1 = (function (){var statearr_15196 = state_15134;(statearr_15196[7] = inst_15083);
return statearr_15196;
})();var statearr_15197_15259 = state_15134__$1;(statearr_15197_15259[2] = null);
(statearr_15197_15259[1] = 32);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 28))
{var inst_15096 = (state_15134[9]);var inst_15075 = (state_15134[11]);var inst_15096__$1 = cljs.core.seq.call(null,inst_15075);var state_15134__$1 = (function (){var statearr_15201 = state_15134;(statearr_15201[9] = inst_15096__$1);
return statearr_15201;
})();if(inst_15096__$1)
{var statearr_15202_15260 = state_15134__$1;(statearr_15202_15260[1] = 33);
} else
{var statearr_15203_15261 = state_15134__$1;(statearr_15203_15261[1] = 34);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 29))
{var inst_15123 = (state_15134[2]);var state_15134__$1 = state_15134;var statearr_15204_15262 = state_15134__$1;(statearr_15204_15262[2] = inst_15123);
(statearr_15204_15262[1] = 26);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 30))
{var inst_15078 = (state_15134[10]);var inst_15075 = (state_15134[11]);var inst_15077 = (state_15134[12]);var inst_15076 = (state_15134[13]);var inst_15092 = (state_15134[2]);var inst_15093 = (inst_15078 + 1);var tmp15198 = inst_15075;var tmp15199 = inst_15077;var tmp15200 = inst_15076;var inst_15075__$1 = tmp15198;var inst_15076__$1 = tmp15200;var inst_15077__$1 = tmp15199;var inst_15078__$1 = inst_15093;var state_15134__$1 = (function (){var statearr_15205 = state_15134;(statearr_15205[10] = inst_15078__$1);
(statearr_15205[11] = inst_15075__$1);
(statearr_15205[12] = inst_15077__$1);
(statearr_15205[13] = inst_15076__$1);
(statearr_15205[30] = inst_15092);
return statearr_15205;
})();var statearr_15206_15263 = state_15134__$1;(statearr_15206_15263[2] = null);
(statearr_15206_15263[1] = 25);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15135 === 31))
{var inst_15083 = (state_15134[7]);var inst_15084 = (state_15134[2]);var inst_15085 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);var inst_15086 = cljs.core.async.untap_STAR_.call(null,m,inst_15083);var state_15134__$1 = (function (){var statearr_15207 = state_15134;(statearr_15207[31] = inst_15084);
(statearr_15207[32] = inst_15085);
return statearr_15207;
})();var statearr_15208_15264 = state_15134__$1;(statearr_15208_15264[2] = inst_15086);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_15134__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_15212 = (new Array(33));(statearr_15212[0] = state_machine__7152__auto__);
(statearr_15212[1] = 1);
return statearr_15212;
});
var state_machine__7152__auto____1 = (function (state_15134){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_15134);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e15213){if((e15213 instanceof Object))
{var ex__7155__auto__ = e15213;var statearr_15214_15265 = state_15134;(statearr_15214_15265[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_15134);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e15213;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__15266 = state_15134;
state_15134 = G__15266;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_15134){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_15134);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_15215 = f__7222__auto__.call(null);(statearr_15215[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto___15216);
return statearr_15215;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return m;
});
/**
* Copies the mult source onto the supplied channel.
* 
* By default the channel will be closed when the source closes,
* but can be determined by the close? parameter.
*/
cljs.core.async.tap = (function() {
var tap = null;
var tap__2 = (function (mult,ch){return tap.call(null,mult,ch,true);
});
var tap__3 = (function (mult,ch,close_QMARK_){cljs.core.async.tap_STAR_.call(null,mult,ch,close_QMARK_);
return ch;
});
tap = function(mult,ch,close_QMARK_){
switch(arguments.length){
case 2:
return tap__2.call(this,mult,ch);
case 3:
return tap__3.call(this,mult,ch,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
tap.cljs$core$IFn$_invoke$arity$2 = tap__2;
tap.cljs$core$IFn$_invoke$arity$3 = tap__3;
return tap;
})()
;
/**
* Disconnects a target channel from a mult
*/
cljs.core.async.untap = (function untap(mult,ch){return cljs.core.async.untap_STAR_.call(null,mult,ch);
});
/**
* Disconnects all target channels from a mult
*/
cljs.core.async.untap_all = (function untap_all(mult){return cljs.core.async.untap_all_STAR_.call(null,mult);
});
cljs.core.async.Mix = {};
cljs.core.async.admix_STAR_ = (function admix_STAR_(m,ch){if((function (){var and__3122__auto__ = m;if(and__3122__auto__)
{return m.cljs$core$async$Mix$admix_STAR_$arity$2;
} else
{return and__3122__auto__;
}
})())
{return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else
{var x__3727__auto__ = (((m == null))?null:m);return (function (){var or__3131__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__3727__auto__)]);if(or__3131__auto__)
{return or__3131__auto__;
} else
{var or__3131__auto____$1 = (cljs.core.async.admix_STAR_["_"]);if(or__3131__auto____$1)
{return or__3131__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.admix*",m);
}
}
})().call(null,m,ch);
}
});
cljs.core.async.unmix_STAR_ = (function unmix_STAR_(m,ch){if((function (){var and__3122__auto__ = m;if(and__3122__auto__)
{return m.cljs$core$async$Mix$unmix_STAR_$arity$2;
} else
{return and__3122__auto__;
}
})())
{return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else
{var x__3727__auto__ = (((m == null))?null:m);return (function (){var or__3131__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__3727__auto__)]);if(or__3131__auto__)
{return or__3131__auto__;
} else
{var or__3131__auto____$1 = (cljs.core.async.unmix_STAR_["_"]);if(or__3131__auto____$1)
{return or__3131__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.unmix*",m);
}
}
})().call(null,m,ch);
}
});
cljs.core.async.unmix_all_STAR_ = (function unmix_all_STAR_(m){if((function (){var and__3122__auto__ = m;if(and__3122__auto__)
{return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1;
} else
{return and__3122__auto__;
}
})())
{return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else
{var x__3727__auto__ = (((m == null))?null:m);return (function (){var or__3131__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__3727__auto__)]);if(or__3131__auto__)
{return or__3131__auto__;
} else
{var or__3131__auto____$1 = (cljs.core.async.unmix_all_STAR_["_"]);if(or__3131__auto____$1)
{return or__3131__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.unmix-all*",m);
}
}
})().call(null,m);
}
});
cljs.core.async.toggle_STAR_ = (function toggle_STAR_(m,state_map){if((function (){var and__3122__auto__ = m;if(and__3122__auto__)
{return m.cljs$core$async$Mix$toggle_STAR_$arity$2;
} else
{return and__3122__auto__;
}
})())
{return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else
{var x__3727__auto__ = (((m == null))?null:m);return (function (){var or__3131__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__3727__auto__)]);if(or__3131__auto__)
{return or__3131__auto__;
} else
{var or__3131__auto____$1 = (cljs.core.async.toggle_STAR_["_"]);if(or__3131__auto____$1)
{return or__3131__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.toggle*",m);
}
}
})().call(null,m,state_map);
}
});
cljs.core.async.solo_mode_STAR_ = (function solo_mode_STAR_(m,mode){if((function (){var and__3122__auto__ = m;if(and__3122__auto__)
{return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2;
} else
{return and__3122__auto__;
}
})())
{return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else
{var x__3727__auto__ = (((m == null))?null:m);return (function (){var or__3131__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__3727__auto__)]);if(or__3131__auto__)
{return or__3131__auto__;
} else
{var or__3131__auto____$1 = (cljs.core.async.solo_mode_STAR_["_"]);if(or__3131__auto____$1)
{return or__3131__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.solo-mode*",m);
}
}
})().call(null,m,mode);
}
});
/**
* Creates and returns a mix of one or more input channels which will
* be put on the supplied out channel. Input sources can be added to
* the mix with 'admix', and removed with 'unmix'. A mix supports
* soloing, muting and pausing multiple inputs atomically using
* 'toggle', and can solo using either muting or pausing as determined
* by 'solo-mode'.
* 
* Each channel can have zero or more boolean modes set via 'toggle':
* 
* :solo - when true, only this (ond other soloed) channel(s) will appear
* in the mix output channel. :mute and :pause states of soloed
* channels are ignored. If solo-mode is :mute, non-soloed
* channels are muted, if :pause, non-soloed channels are
* paused.
* 
* :mute - muted channels will have their contents consumed but not included in the mix
* :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
*/
cljs.core.async.mix = (function mix(out){var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);var solo_modes = cljs.core.PersistentHashSet.fromArray([new cljs.core.Keyword(null,"pause","pause",1120344424),null,new cljs.core.Keyword(null,"mute","mute",1017267595),null], true);var attrs = cljs.core.conj.call(null,solo_modes,new cljs.core.Keyword(null,"solo","solo",1017440337));var solo_mode = cljs.core.atom.call(null,new cljs.core.Keyword(null,"mute","mute",1017267595));var change = cljs.core.async.chan.call(null);var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){return cljs.core.async.put_BANG_.call(null,change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){return cljs.core.reduce_kv.call(null,((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){if(cljs.core.truth_(attr.call(null,v)))
{return cljs.core.conj.call(null,ret,c);
} else
{return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){var chs = cljs.core.deref.call(null,cs);var mode = cljs.core.deref.call(null,solo_mode);var solos = pick.call(null,new cljs.core.Keyword(null,"solo","solo",1017440337),chs);var pauses = pick.call(null,new cljs.core.Keyword(null,"pause","pause",1120344424),chs);return cljs.core.PersistentArrayMap.fromArray([new cljs.core.Keyword(null,"solos","solos",1123523302),solos,new cljs.core.Keyword(null,"mutes","mutes",1118168300),pick.call(null,new cljs.core.Keyword(null,"mute","mute",1017267595),chs),new cljs.core.Keyword(null,"reads","reads",1122290959),cljs.core.conj.call(null,(((cljs.core._EQ_.call(null,mode,new cljs.core.Keyword(null,"pause","pause",1120344424))) && (!(cljs.core.empty_QMARK_.call(null,solos))))?cljs.core.vec.call(null,solos):cljs.core.vec.call(null,cljs.core.remove.call(null,pauses,cljs.core.keys.call(null,chs)))),change)], true);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;var m = (function (){if(typeof cljs.core.async.t15376 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t15376 = (function (pick,out,attrs,cs,calc_state,solo_modes,mix,changed,change,solo_mode,meta15377){
this.pick = pick;
this.out = out;
this.attrs = attrs;
this.cs = cs;
this.calc_state = calc_state;
this.solo_modes = solo_modes;
this.mix = mix;
this.changed = changed;
this.change = change;
this.solo_mode = solo_mode;
this.meta15377 = meta15377;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t15376.cljs$lang$type = true;
cljs.core.async.t15376.cljs$lang$ctorStr = "cljs.core.async/t15376";
cljs.core.async.t15376.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__3668__auto__,writer__3669__auto__,opt__3670__auto__){return cljs.core._write.call(null,writer__3669__auto__,"cljs.core.async/t15376");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t15376.prototype.cljs$core$async$Mix$ = true;
cljs.core.async.t15376.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t15376.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t15376.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){var self__ = this;
var ___$1 = this;cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t15376.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core.merge),state_map);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t15376.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){var self__ = this;
var ___$1 = this;if(cljs.core.truth_(self__.solo_modes.call(null,mode)))
{} else
{throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str([cljs.core.str("mode must be one of: "),cljs.core.str(self__.solo_modes)].join('')),cljs.core.str("\n"),cljs.core.str(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"solo-modes","solo-modes",-1162732933,null),new cljs.core.Symbol(null,"mode","mode",-1637174436,null))))].join('')));
}
cljs.core.reset_BANG_.call(null,self__.solo_mode,mode);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t15376.prototype.cljs$core$async$Mux$ = true;
cljs.core.async.t15376.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){var self__ = this;
var ___$1 = this;return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t15376.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_15378){var self__ = this;
var _15378__$1 = this;return self__.meta15377;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t15376.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_15378,meta15377__$1){var self__ = this;
var _15378__$1 = this;return (new cljs.core.async.t15376(self__.pick,self__.out,self__.attrs,self__.cs,self__.calc_state,self__.solo_modes,self__.mix,self__.changed,self__.change,self__.solo_mode,meta15377__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.__GT_t15376 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function __GT_t15376(pick__$1,out__$1,attrs__$1,cs__$1,calc_state__$1,solo_modes__$1,mix__$1,changed__$1,change__$1,solo_mode__$1,meta15377){return (new cljs.core.async.t15376(pick__$1,out__$1,attrs__$1,cs__$1,calc_state__$1,solo_modes__$1,mix__$1,changed__$1,change__$1,solo_mode__$1,meta15377));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
}
return (new cljs.core.async.t15376(pick,out,attrs,cs,calc_state,solo_modes,mix,changed,change,solo_mode,null));
})();var c__7221__auto___15485 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_15443){var state_val_15444 = (state_15443[1]);if((state_val_15444 === 1))
{var inst_15382 = (state_15443[7]);var inst_15382__$1 = calc_state.call(null);var inst_15383 = cljs.core.seq_QMARK_.call(null,inst_15382__$1);var state_15443__$1 = (function (){var statearr_15445 = state_15443;(statearr_15445[7] = inst_15382__$1);
return statearr_15445;
})();if(inst_15383)
{var statearr_15446_15486 = state_15443__$1;(statearr_15446_15486[1] = 2);
} else
{var statearr_15447_15487 = state_15443__$1;(statearr_15447_15487[1] = 3);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 2))
{var inst_15382 = (state_15443[7]);var inst_15385 = cljs.core.apply.call(null,cljs.core.hash_map,inst_15382);var state_15443__$1 = state_15443;var statearr_15448_15488 = state_15443__$1;(statearr_15448_15488[2] = inst_15385);
(statearr_15448_15488[1] = 4);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 3))
{var inst_15382 = (state_15443[7]);var state_15443__$1 = state_15443;var statearr_15449_15489 = state_15443__$1;(statearr_15449_15489[2] = inst_15382);
(statearr_15449_15489[1] = 4);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 4))
{var inst_15382 = (state_15443[7]);var inst_15388 = (state_15443[2]);var inst_15389 = cljs.core.get.call(null,inst_15388,new cljs.core.Keyword(null,"reads","reads",1122290959));var inst_15390 = cljs.core.get.call(null,inst_15388,new cljs.core.Keyword(null,"mutes","mutes",1118168300));var inst_15391 = cljs.core.get.call(null,inst_15388,new cljs.core.Keyword(null,"solos","solos",1123523302));var inst_15392 = inst_15382;var state_15443__$1 = (function (){var statearr_15450 = state_15443;(statearr_15450[8] = inst_15389);
(statearr_15450[9] = inst_15390);
(statearr_15450[10] = inst_15391);
(statearr_15450[11] = inst_15392);
return statearr_15450;
})();var statearr_15451_15490 = state_15443__$1;(statearr_15451_15490[2] = null);
(statearr_15451_15490[1] = 5);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 5))
{var inst_15392 = (state_15443[11]);var inst_15395 = cljs.core.seq_QMARK_.call(null,inst_15392);var state_15443__$1 = state_15443;if(inst_15395)
{var statearr_15452_15491 = state_15443__$1;(statearr_15452_15491[1] = 7);
} else
{var statearr_15453_15492 = state_15443__$1;(statearr_15453_15492[1] = 8);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 6))
{var inst_15441 = (state_15443[2]);var state_15443__$1 = state_15443;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_15443__$1,inst_15441);
} else
{if((state_val_15444 === 7))
{var inst_15392 = (state_15443[11]);var inst_15397 = cljs.core.apply.call(null,cljs.core.hash_map,inst_15392);var state_15443__$1 = state_15443;var statearr_15454_15493 = state_15443__$1;(statearr_15454_15493[2] = inst_15397);
(statearr_15454_15493[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 8))
{var inst_15392 = (state_15443[11]);var state_15443__$1 = state_15443;var statearr_15455_15494 = state_15443__$1;(statearr_15455_15494[2] = inst_15392);
(statearr_15455_15494[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 9))
{var inst_15400 = (state_15443[12]);var inst_15400__$1 = (state_15443[2]);var inst_15401 = cljs.core.get.call(null,inst_15400__$1,new cljs.core.Keyword(null,"reads","reads",1122290959));var inst_15402 = cljs.core.get.call(null,inst_15400__$1,new cljs.core.Keyword(null,"mutes","mutes",1118168300));var inst_15403 = cljs.core.get.call(null,inst_15400__$1,new cljs.core.Keyword(null,"solos","solos",1123523302));var state_15443__$1 = (function (){var statearr_15456 = state_15443;(statearr_15456[13] = inst_15403);
(statearr_15456[14] = inst_15402);
(statearr_15456[12] = inst_15400__$1);
return statearr_15456;
})();return cljs.core.async.impl.ioc_helpers.ioc_alts_BANG_.call(null,state_15443__$1,10,inst_15401);
} else
{if((state_val_15444 === 10))
{var inst_15407 = (state_15443[15]);var inst_15408 = (state_15443[16]);var inst_15406 = (state_15443[2]);var inst_15407__$1 = cljs.core.nth.call(null,inst_15406,0,null);var inst_15408__$1 = cljs.core.nth.call(null,inst_15406,1,null);var inst_15409 = (inst_15407__$1 == null);var inst_15410 = cljs.core._EQ_.call(null,inst_15408__$1,change);var inst_15411 = (inst_15409) || (inst_15410);var state_15443__$1 = (function (){var statearr_15457 = state_15443;(statearr_15457[15] = inst_15407__$1);
(statearr_15457[16] = inst_15408__$1);
return statearr_15457;
})();if(cljs.core.truth_(inst_15411))
{var statearr_15458_15495 = state_15443__$1;(statearr_15458_15495[1] = 11);
} else
{var statearr_15459_15496 = state_15443__$1;(statearr_15459_15496[1] = 12);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 11))
{var inst_15407 = (state_15443[15]);var inst_15413 = (inst_15407 == null);var state_15443__$1 = state_15443;if(cljs.core.truth_(inst_15413))
{var statearr_15460_15497 = state_15443__$1;(statearr_15460_15497[1] = 14);
} else
{var statearr_15461_15498 = state_15443__$1;(statearr_15461_15498[1] = 15);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 12))
{var inst_15422 = (state_15443[17]);var inst_15408 = (state_15443[16]);var inst_15403 = (state_15443[13]);var inst_15422__$1 = inst_15403.call(null,inst_15408);var state_15443__$1 = (function (){var statearr_15462 = state_15443;(statearr_15462[17] = inst_15422__$1);
return statearr_15462;
})();if(cljs.core.truth_(inst_15422__$1))
{var statearr_15463_15499 = state_15443__$1;(statearr_15463_15499[1] = 17);
} else
{var statearr_15464_15500 = state_15443__$1;(statearr_15464_15500[1] = 18);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 13))
{var inst_15439 = (state_15443[2]);var state_15443__$1 = state_15443;var statearr_15465_15501 = state_15443__$1;(statearr_15465_15501[2] = inst_15439);
(statearr_15465_15501[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 14))
{var inst_15408 = (state_15443[16]);var inst_15415 = cljs.core.swap_BANG_.call(null,cs,cljs.core.dissoc,inst_15408);var state_15443__$1 = state_15443;var statearr_15466_15502 = state_15443__$1;(statearr_15466_15502[2] = inst_15415);
(statearr_15466_15502[1] = 16);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 15))
{var state_15443__$1 = state_15443;var statearr_15467_15503 = state_15443__$1;(statearr_15467_15503[2] = null);
(statearr_15467_15503[1] = 16);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 16))
{var inst_15418 = (state_15443[2]);var inst_15419 = calc_state.call(null);var inst_15392 = inst_15419;var state_15443__$1 = (function (){var statearr_15468 = state_15443;(statearr_15468[18] = inst_15418);
(statearr_15468[11] = inst_15392);
return statearr_15468;
})();var statearr_15469_15504 = state_15443__$1;(statearr_15469_15504[2] = null);
(statearr_15469_15504[1] = 5);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 17))
{var inst_15422 = (state_15443[17]);var state_15443__$1 = state_15443;var statearr_15470_15505 = state_15443__$1;(statearr_15470_15505[2] = inst_15422);
(statearr_15470_15505[1] = 19);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 18))
{var inst_15408 = (state_15443[16]);var inst_15403 = (state_15443[13]);var inst_15402 = (state_15443[14]);var inst_15425 = cljs.core.empty_QMARK_.call(null,inst_15403);var inst_15426 = inst_15402.call(null,inst_15408);var inst_15427 = cljs.core.not.call(null,inst_15426);var inst_15428 = (inst_15425) && (inst_15427);var state_15443__$1 = state_15443;var statearr_15471_15506 = state_15443__$1;(statearr_15471_15506[2] = inst_15428);
(statearr_15471_15506[1] = 19);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 19))
{var inst_15430 = (state_15443[2]);var state_15443__$1 = state_15443;if(cljs.core.truth_(inst_15430))
{var statearr_15472_15507 = state_15443__$1;(statearr_15472_15507[1] = 20);
} else
{var statearr_15473_15508 = state_15443__$1;(statearr_15473_15508[1] = 21);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 20))
{var inst_15407 = (state_15443[15]);var state_15443__$1 = state_15443;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_15443__$1,23,out,inst_15407);
} else
{if((state_val_15444 === 21))
{var state_15443__$1 = state_15443;var statearr_15474_15509 = state_15443__$1;(statearr_15474_15509[2] = null);
(statearr_15474_15509[1] = 22);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 22))
{var inst_15400 = (state_15443[12]);var inst_15436 = (state_15443[2]);var inst_15392 = inst_15400;var state_15443__$1 = (function (){var statearr_15475 = state_15443;(statearr_15475[11] = inst_15392);
(statearr_15475[19] = inst_15436);
return statearr_15475;
})();var statearr_15476_15510 = state_15443__$1;(statearr_15476_15510[2] = null);
(statearr_15476_15510[1] = 5);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15444 === 23))
{var inst_15433 = (state_15443[2]);var state_15443__$1 = state_15443;var statearr_15477_15511 = state_15443__$1;(statearr_15477_15511[2] = inst_15433);
(statearr_15477_15511[1] = 22);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_15481 = (new Array(20));(statearr_15481[0] = state_machine__7152__auto__);
(statearr_15481[1] = 1);
return statearr_15481;
});
var state_machine__7152__auto____1 = (function (state_15443){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_15443);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e15482){if((e15482 instanceof Object))
{var ex__7155__auto__ = e15482;var statearr_15483_15512 = state_15443;(statearr_15483_15512[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_15443);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e15482;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__15513 = state_15443;
state_15443 = G__15513;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_15443){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_15443);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_15484 = f__7222__auto__.call(null);(statearr_15484[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto___15485);
return statearr_15484;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return m;
});
/**
* Adds ch as an input to the mix
*/
cljs.core.async.admix = (function admix(mix,ch){return cljs.core.async.admix_STAR_.call(null,mix,ch);
});
/**
* Removes ch as an input to the mix
*/
cljs.core.async.unmix = (function unmix(mix,ch){return cljs.core.async.unmix_STAR_.call(null,mix,ch);
});
/**
* removes all inputs from the mix
*/
cljs.core.async.unmix_all = (function unmix_all(mix){return cljs.core.async.unmix_all_STAR_.call(null,mix);
});
/**
* Atomically sets the state(s) of one or more channels in a mix. The
* state map is a map of channels -> channel-state-map. A
* channel-state-map is a map of attrs -> boolean, where attr is one or
* more of :mute, :pause or :solo. Any states supplied are merged with
* the current state.
* 
* Note that channels can be added to a mix via toggle, which can be
* used to add channels in a particular (e.g. paused) state.
*/
cljs.core.async.toggle = (function toggle(mix,state_map){return cljs.core.async.toggle_STAR_.call(null,mix,state_map);
});
/**
* Sets the solo mode of the mix. mode must be one of :mute or :pause
*/
cljs.core.async.solo_mode = (function solo_mode(mix,mode){return cljs.core.async.solo_mode_STAR_.call(null,mix,mode);
});
cljs.core.async.Pub = {};
cljs.core.async.sub_STAR_ = (function sub_STAR_(p,v,ch,close_QMARK_){if((function (){var and__3122__auto__ = p;if(and__3122__auto__)
{return p.cljs$core$async$Pub$sub_STAR_$arity$4;
} else
{return and__3122__auto__;
}
})())
{return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else
{var x__3727__auto__ = (((p == null))?null:p);return (function (){var or__3131__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__3727__auto__)]);if(or__3131__auto__)
{return or__3131__auto__;
} else
{var or__3131__auto____$1 = (cljs.core.async.sub_STAR_["_"]);if(or__3131__auto____$1)
{return or__3131__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Pub.sub*",p);
}
}
})().call(null,p,v,ch,close_QMARK_);
}
});
cljs.core.async.unsub_STAR_ = (function unsub_STAR_(p,v,ch){if((function (){var and__3122__auto__ = p;if(and__3122__auto__)
{return p.cljs$core$async$Pub$unsub_STAR_$arity$3;
} else
{return and__3122__auto__;
}
})())
{return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else
{var x__3727__auto__ = (((p == null))?null:p);return (function (){var or__3131__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__3727__auto__)]);if(or__3131__auto__)
{return or__3131__auto__;
} else
{var or__3131__auto____$1 = (cljs.core.async.unsub_STAR_["_"]);if(or__3131__auto____$1)
{return or__3131__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Pub.unsub*",p);
}
}
})().call(null,p,v,ch);
}
});
cljs.core.async.unsub_all_STAR_ = (function() {
var unsub_all_STAR_ = null;
var unsub_all_STAR___1 = (function (p){if((function (){var and__3122__auto__ = p;if(and__3122__auto__)
{return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1;
} else
{return and__3122__auto__;
}
})())
{return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else
{var x__3727__auto__ = (((p == null))?null:p);return (function (){var or__3131__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__3727__auto__)]);if(or__3131__auto__)
{return or__3131__auto__;
} else
{var or__3131__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);if(or__3131__auto____$1)
{return or__3131__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
})().call(null,p);
}
});
var unsub_all_STAR___2 = (function (p,v){if((function (){var and__3122__auto__ = p;if(and__3122__auto__)
{return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2;
} else
{return and__3122__auto__;
}
})())
{return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else
{var x__3727__auto__ = (((p == null))?null:p);return (function (){var or__3131__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__3727__auto__)]);if(or__3131__auto__)
{return or__3131__auto__;
} else
{var or__3131__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);if(or__3131__auto____$1)
{return or__3131__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
})().call(null,p,v);
}
});
unsub_all_STAR_ = function(p,v){
switch(arguments.length){
case 1:
return unsub_all_STAR___1.call(this,p);
case 2:
return unsub_all_STAR___2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = unsub_all_STAR___1;
unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = unsub_all_STAR___2;
return unsub_all_STAR_;
})()
;
/**
* Creates and returns a pub(lication) of the supplied channel,
* partitioned into topics by the topic-fn. topic-fn will be applied to
* each value on the channel and the result will determine the 'topic'
* on which that value will be put. Channels can be subscribed to
* receive copies of topics using 'sub', and unsubscribed using
* 'unsub'. Each topic will be handled by an internal mult on a
* dedicated channel. By default these internal channels are
* unbuffered, but a buf-fn can be supplied which, given a topic,
* creates a buffer with desired properties.
* 
* Each item is distributed to all subs in parallel and synchronously,
* i.e. each sub must accept before the next item is distributed. Use
* buffering/windowing to prevent slow subs from holding up the pub.
* 
* Note that if buf-fns are used then each topic is handled
* asynchronously, i.e. if a channel is subscribed to more than one
* topic it should not expect them to be interleaved identically with
* the source.
*/
cljs.core.async.pub = (function() {
var pub = null;
var pub__2 = (function (ch,topic_fn){return pub.call(null,ch,topic_fn,cljs.core.constantly.call(null,null));
});
var pub__3 = (function (ch,topic_fn,buf_fn){var mults = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);var ensure_mult = ((function (mults){
return (function (topic){var or__3131__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,mults),topic);if(cljs.core.truth_(or__3131__auto__))
{return or__3131__auto__;
} else
{return cljs.core.get.call(null,cljs.core.swap_BANG_.call(null,mults,((function (or__3131__auto__,mults){
return (function (p1__15514_SHARP_){if(cljs.core.truth_(p1__15514_SHARP_.call(null,topic)))
{return p1__15514_SHARP_;
} else
{return cljs.core.assoc.call(null,p1__15514_SHARP_,topic,cljs.core.async.mult.call(null,cljs.core.async.chan.call(null,buf_fn.call(null,topic))));
}
});})(or__3131__auto__,mults))
),topic);
}
});})(mults))
;var p = (function (){if(typeof cljs.core.async.t15639 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t15639 = (function (ensure_mult,mults,buf_fn,topic_fn,ch,pub,meta15640){
this.ensure_mult = ensure_mult;
this.mults = mults;
this.buf_fn = buf_fn;
this.topic_fn = topic_fn;
this.ch = ch;
this.pub = pub;
this.meta15640 = meta15640;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t15639.cljs$lang$type = true;
cljs.core.async.t15639.cljs$lang$ctorStr = "cljs.core.async/t15639";
cljs.core.async.t15639.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__3668__auto__,writer__3669__auto__,opt__3670__auto__){return cljs.core._write.call(null,writer__3669__auto__,"cljs.core.async/t15639");
});})(mults,ensure_mult))
;
cljs.core.async.t15639.prototype.cljs$core$async$Pub$ = true;
cljs.core.async.t15639.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$2,close_QMARK_){var self__ = this;
var p__$1 = this;var m = self__.ensure_mult.call(null,topic);return cljs.core.async.tap.call(null,m,ch__$2,close_QMARK_);
});})(mults,ensure_mult))
;
cljs.core.async.t15639.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$2){var self__ = this;
var p__$1 = this;var temp__4092__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,self__.mults),topic);if(cljs.core.truth_(temp__4092__auto__))
{var m = temp__4092__auto__;return cljs.core.async.untap.call(null,m,ch__$2);
} else
{return null;
}
});})(mults,ensure_mult))
;
cljs.core.async.t15639.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){var self__ = this;
var ___$1 = this;return cljs.core.reset_BANG_.call(null,self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;
cljs.core.async.t15639.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){var self__ = this;
var ___$1 = this;return cljs.core.swap_BANG_.call(null,self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;
cljs.core.async.t15639.prototype.cljs$core$async$Mux$ = true;
cljs.core.async.t15639.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){var self__ = this;
var ___$1 = this;return self__.ch;
});})(mults,ensure_mult))
;
cljs.core.async.t15639.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_15641){var self__ = this;
var _15641__$1 = this;return self__.meta15640;
});})(mults,ensure_mult))
;
cljs.core.async.t15639.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_15641,meta15640__$1){var self__ = this;
var _15641__$1 = this;return (new cljs.core.async.t15639(self__.ensure_mult,self__.mults,self__.buf_fn,self__.topic_fn,self__.ch,self__.pub,meta15640__$1));
});})(mults,ensure_mult))
;
cljs.core.async.__GT_t15639 = ((function (mults,ensure_mult){
return (function __GT_t15639(ensure_mult__$1,mults__$1,buf_fn__$1,topic_fn__$1,ch__$1,pub__$1,meta15640){return (new cljs.core.async.t15639(ensure_mult__$1,mults__$1,buf_fn__$1,topic_fn__$1,ch__$1,pub__$1,meta15640));
});})(mults,ensure_mult))
;
}
return (new cljs.core.async.t15639(ensure_mult,mults,buf_fn,topic_fn,ch,pub,null));
})();var c__7221__auto___15763 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_15715){var state_val_15716 = (state_15715[1]);if((state_val_15716 === 1))
{var state_15715__$1 = state_15715;var statearr_15717_15764 = state_15715__$1;(statearr_15717_15764[2] = null);
(statearr_15717_15764[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 2))
{var state_15715__$1 = state_15715;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_15715__$1,4,ch);
} else
{if((state_val_15716 === 3))
{var inst_15713 = (state_15715[2]);var state_15715__$1 = state_15715;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_15715__$1,inst_15713);
} else
{if((state_val_15716 === 4))
{var inst_15644 = (state_15715[7]);var inst_15644__$1 = (state_15715[2]);var inst_15645 = (inst_15644__$1 == null);var state_15715__$1 = (function (){var statearr_15718 = state_15715;(statearr_15718[7] = inst_15644__$1);
return statearr_15718;
})();if(cljs.core.truth_(inst_15645))
{var statearr_15719_15765 = state_15715__$1;(statearr_15719_15765[1] = 5);
} else
{var statearr_15720_15766 = state_15715__$1;(statearr_15720_15766[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 5))
{var inst_15651 = cljs.core.deref.call(null,mults);var inst_15652 = cljs.core.vals.call(null,inst_15651);var inst_15653 = cljs.core.seq.call(null,inst_15652);var inst_15654 = inst_15653;var inst_15655 = null;var inst_15656 = 0;var inst_15657 = 0;var state_15715__$1 = (function (){var statearr_15721 = state_15715;(statearr_15721[8] = inst_15657);
(statearr_15721[9] = inst_15656);
(statearr_15721[10] = inst_15655);
(statearr_15721[11] = inst_15654);
return statearr_15721;
})();var statearr_15722_15767 = state_15715__$1;(statearr_15722_15767[2] = null);
(statearr_15722_15767[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 6))
{var inst_15694 = (state_15715[12]);var inst_15692 = (state_15715[13]);var inst_15644 = (state_15715[7]);var inst_15692__$1 = topic_fn.call(null,inst_15644);var inst_15693 = cljs.core.deref.call(null,mults);var inst_15694__$1 = cljs.core.get.call(null,inst_15693,inst_15692__$1);var state_15715__$1 = (function (){var statearr_15723 = state_15715;(statearr_15723[12] = inst_15694__$1);
(statearr_15723[13] = inst_15692__$1);
return statearr_15723;
})();if(cljs.core.truth_(inst_15694__$1))
{var statearr_15724_15768 = state_15715__$1;(statearr_15724_15768[1] = 19);
} else
{var statearr_15725_15769 = state_15715__$1;(statearr_15725_15769[1] = 20);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 7))
{var inst_15711 = (state_15715[2]);var state_15715__$1 = state_15715;var statearr_15726_15770 = state_15715__$1;(statearr_15726_15770[2] = inst_15711);
(statearr_15726_15770[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 8))
{var inst_15657 = (state_15715[8]);var inst_15656 = (state_15715[9]);var inst_15659 = (inst_15657 < inst_15656);var inst_15660 = inst_15659;var state_15715__$1 = state_15715;if(cljs.core.truth_(inst_15660))
{var statearr_15730_15771 = state_15715__$1;(statearr_15730_15771[1] = 10);
} else
{var statearr_15731_15772 = state_15715__$1;(statearr_15731_15772[1] = 11);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 9))
{var inst_15690 = (state_15715[2]);var state_15715__$1 = state_15715;var statearr_15732_15773 = state_15715__$1;(statearr_15732_15773[2] = inst_15690);
(statearr_15732_15773[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 10))
{var inst_15657 = (state_15715[8]);var inst_15656 = (state_15715[9]);var inst_15655 = (state_15715[10]);var inst_15654 = (state_15715[11]);var inst_15662 = cljs.core._nth.call(null,inst_15655,inst_15657);var inst_15663 = cljs.core.async.muxch_STAR_.call(null,inst_15662);var inst_15664 = cljs.core.async.close_BANG_.call(null,inst_15663);var inst_15665 = (inst_15657 + 1);var tmp15727 = inst_15656;var tmp15728 = inst_15655;var tmp15729 = inst_15654;var inst_15654__$1 = tmp15729;var inst_15655__$1 = tmp15728;var inst_15656__$1 = tmp15727;var inst_15657__$1 = inst_15665;var state_15715__$1 = (function (){var statearr_15733 = state_15715;(statearr_15733[14] = inst_15664);
(statearr_15733[8] = inst_15657__$1);
(statearr_15733[9] = inst_15656__$1);
(statearr_15733[10] = inst_15655__$1);
(statearr_15733[11] = inst_15654__$1);
return statearr_15733;
})();var statearr_15734_15774 = state_15715__$1;(statearr_15734_15774[2] = null);
(statearr_15734_15774[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 11))
{var inst_15668 = (state_15715[15]);var inst_15654 = (state_15715[11]);var inst_15668__$1 = cljs.core.seq.call(null,inst_15654);var state_15715__$1 = (function (){var statearr_15735 = state_15715;(statearr_15735[15] = inst_15668__$1);
return statearr_15735;
})();if(inst_15668__$1)
{var statearr_15736_15775 = state_15715__$1;(statearr_15736_15775[1] = 13);
} else
{var statearr_15737_15776 = state_15715__$1;(statearr_15737_15776[1] = 14);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 12))
{var inst_15688 = (state_15715[2]);var state_15715__$1 = state_15715;var statearr_15738_15777 = state_15715__$1;(statearr_15738_15777[2] = inst_15688);
(statearr_15738_15777[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 13))
{var inst_15668 = (state_15715[15]);var inst_15670 = cljs.core.chunked_seq_QMARK_.call(null,inst_15668);var state_15715__$1 = state_15715;if(inst_15670)
{var statearr_15739_15778 = state_15715__$1;(statearr_15739_15778[1] = 16);
} else
{var statearr_15740_15779 = state_15715__$1;(statearr_15740_15779[1] = 17);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 14))
{var state_15715__$1 = state_15715;var statearr_15741_15780 = state_15715__$1;(statearr_15741_15780[2] = null);
(statearr_15741_15780[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 15))
{var inst_15686 = (state_15715[2]);var state_15715__$1 = state_15715;var statearr_15742_15781 = state_15715__$1;(statearr_15742_15781[2] = inst_15686);
(statearr_15742_15781[1] = 12);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 16))
{var inst_15668 = (state_15715[15]);var inst_15672 = cljs.core.chunk_first.call(null,inst_15668);var inst_15673 = cljs.core.chunk_rest.call(null,inst_15668);var inst_15674 = cljs.core.count.call(null,inst_15672);var inst_15654 = inst_15673;var inst_15655 = inst_15672;var inst_15656 = inst_15674;var inst_15657 = 0;var state_15715__$1 = (function (){var statearr_15743 = state_15715;(statearr_15743[8] = inst_15657);
(statearr_15743[9] = inst_15656);
(statearr_15743[10] = inst_15655);
(statearr_15743[11] = inst_15654);
return statearr_15743;
})();var statearr_15744_15782 = state_15715__$1;(statearr_15744_15782[2] = null);
(statearr_15744_15782[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 17))
{var inst_15668 = (state_15715[15]);var inst_15677 = cljs.core.first.call(null,inst_15668);var inst_15678 = cljs.core.async.muxch_STAR_.call(null,inst_15677);var inst_15679 = cljs.core.async.close_BANG_.call(null,inst_15678);var inst_15680 = cljs.core.next.call(null,inst_15668);var inst_15654 = inst_15680;var inst_15655 = null;var inst_15656 = 0;var inst_15657 = 0;var state_15715__$1 = (function (){var statearr_15745 = state_15715;(statearr_15745[8] = inst_15657);
(statearr_15745[9] = inst_15656);
(statearr_15745[10] = inst_15655);
(statearr_15745[11] = inst_15654);
(statearr_15745[16] = inst_15679);
return statearr_15745;
})();var statearr_15746_15783 = state_15715__$1;(statearr_15746_15783[2] = null);
(statearr_15746_15783[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 18))
{var inst_15683 = (state_15715[2]);var state_15715__$1 = state_15715;var statearr_15747_15784 = state_15715__$1;(statearr_15747_15784[2] = inst_15683);
(statearr_15747_15784[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 19))
{var state_15715__$1 = state_15715;var statearr_15748_15785 = state_15715__$1;(statearr_15748_15785[2] = null);
(statearr_15748_15785[1] = 24);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 20))
{var state_15715__$1 = state_15715;var statearr_15749_15786 = state_15715__$1;(statearr_15749_15786[2] = null);
(statearr_15749_15786[1] = 21);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 21))
{var inst_15708 = (state_15715[2]);var state_15715__$1 = (function (){var statearr_15750 = state_15715;(statearr_15750[17] = inst_15708);
return statearr_15750;
})();var statearr_15751_15787 = state_15715__$1;(statearr_15751_15787[2] = null);
(statearr_15751_15787[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 22))
{var inst_15705 = (state_15715[2]);var state_15715__$1 = state_15715;var statearr_15752_15788 = state_15715__$1;(statearr_15752_15788[2] = inst_15705);
(statearr_15752_15788[1] = 21);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 23))
{var inst_15692 = (state_15715[13]);var inst_15696 = (state_15715[2]);var inst_15697 = cljs.core.swap_BANG_.call(null,mults,cljs.core.dissoc,inst_15692);var state_15715__$1 = (function (){var statearr_15753 = state_15715;(statearr_15753[18] = inst_15696);
return statearr_15753;
})();var statearr_15754_15789 = state_15715__$1;(statearr_15754_15789[2] = inst_15697);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_15715__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15716 === 24))
{var inst_15694 = (state_15715[12]);var inst_15644 = (state_15715[7]);var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_15715,23,Object,null,22);var inst_15701 = cljs.core.async.muxch_STAR_.call(null,inst_15694);var state_15715__$1 = state_15715;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_15715__$1,25,inst_15701,inst_15644);
} else
{if((state_val_15716 === 25))
{var inst_15703 = (state_15715[2]);var state_15715__$1 = state_15715;var statearr_15755_15790 = state_15715__$1;(statearr_15755_15790[2] = inst_15703);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_15715__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_15759 = (new Array(19));(statearr_15759[0] = state_machine__7152__auto__);
(statearr_15759[1] = 1);
return statearr_15759;
});
var state_machine__7152__auto____1 = (function (state_15715){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_15715);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e15760){if((e15760 instanceof Object))
{var ex__7155__auto__ = e15760;var statearr_15761_15791 = state_15715;(statearr_15761_15791[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_15715);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e15760;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__15792 = state_15715;
state_15715 = G__15792;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_15715){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_15715);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_15762 = f__7222__auto__.call(null);(statearr_15762[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto___15763);
return statearr_15762;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return p;
});
pub = function(ch,topic_fn,buf_fn){
switch(arguments.length){
case 2:
return pub__2.call(this,ch,topic_fn);
case 3:
return pub__3.call(this,ch,topic_fn,buf_fn);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
pub.cljs$core$IFn$_invoke$arity$2 = pub__2;
pub.cljs$core$IFn$_invoke$arity$3 = pub__3;
return pub;
})()
;
/**
* Subscribes a channel to a topic of a pub.
* 
* By default the channel will be closed when the source closes,
* but can be determined by the close? parameter.
*/
cljs.core.async.sub = (function() {
var sub = null;
var sub__3 = (function (p,topic,ch){return sub.call(null,p,topic,ch,true);
});
var sub__4 = (function (p,topic,ch,close_QMARK_){return cljs.core.async.sub_STAR_.call(null,p,topic,ch,close_QMARK_);
});
sub = function(p,topic,ch,close_QMARK_){
switch(arguments.length){
case 3:
return sub__3.call(this,p,topic,ch);
case 4:
return sub__4.call(this,p,topic,ch,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
sub.cljs$core$IFn$_invoke$arity$3 = sub__3;
sub.cljs$core$IFn$_invoke$arity$4 = sub__4;
return sub;
})()
;
/**
* Unsubscribes a channel from a topic of a pub
*/
cljs.core.async.unsub = (function unsub(p,topic,ch){return cljs.core.async.unsub_STAR_.call(null,p,topic,ch);
});
/**
* Unsubscribes all channels from a pub, or a topic of a pub
*/
cljs.core.async.unsub_all = (function() {
var unsub_all = null;
var unsub_all__1 = (function (p){return cljs.core.async.unsub_all_STAR_.call(null,p);
});
var unsub_all__2 = (function (p,topic){return cljs.core.async.unsub_all_STAR_.call(null,p,topic);
});
unsub_all = function(p,topic){
switch(arguments.length){
case 1:
return unsub_all__1.call(this,p);
case 2:
return unsub_all__2.call(this,p,topic);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
unsub_all.cljs$core$IFn$_invoke$arity$1 = unsub_all__1;
unsub_all.cljs$core$IFn$_invoke$arity$2 = unsub_all__2;
return unsub_all;
})()
;
/**
* Takes a function and a collection of source channels, and returns a
* channel which contains the values produced by applying f to the set
* of first items taken from each source channel, followed by applying
* f to the set of second items from each channel, until any one of the
* channels is closed, at which point the output channel will be
* closed. The returned channel will be unbuffered by default, or a
* buf-or-n can be supplied
*/
cljs.core.async.map = (function() {
var map = null;
var map__2 = (function (f,chs){return map.call(null,f,chs,null);
});
var map__3 = (function (f,chs,buf_or_n){var chs__$1 = cljs.core.vec.call(null,chs);var out = cljs.core.async.chan.call(null,buf_or_n);var cnt = cljs.core.count.call(null,chs__$1);var rets = cljs.core.object_array.call(null,cnt);var dchan = cljs.core.async.chan.call(null,1);var dctr = cljs.core.atom.call(null,null);var done = cljs.core.mapv.call(null,((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){(rets[i] = ret);
if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === 0))
{return cljs.core.async.put_BANG_.call(null,dchan,rets.slice(0));
} else
{return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.call(null,cnt));var c__7221__auto___15929 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_15899){var state_val_15900 = (state_15899[1]);if((state_val_15900 === 1))
{var state_15899__$1 = state_15899;var statearr_15901_15930 = state_15899__$1;(statearr_15901_15930[2] = null);
(statearr_15901_15930[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15900 === 2))
{var inst_15862 = cljs.core.reset_BANG_.call(null,dctr,cnt);var inst_15863 = 0;var state_15899__$1 = (function (){var statearr_15902 = state_15899;(statearr_15902[7] = inst_15863);
(statearr_15902[8] = inst_15862);
return statearr_15902;
})();var statearr_15903_15931 = state_15899__$1;(statearr_15903_15931[2] = null);
(statearr_15903_15931[1] = 4);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15900 === 3))
{var inst_15897 = (state_15899[2]);var state_15899__$1 = state_15899;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_15899__$1,inst_15897);
} else
{if((state_val_15900 === 4))
{var inst_15863 = (state_15899[7]);var inst_15865 = (inst_15863 < cnt);var state_15899__$1 = state_15899;if(cljs.core.truth_(inst_15865))
{var statearr_15904_15932 = state_15899__$1;(statearr_15904_15932[1] = 6);
} else
{var statearr_15905_15933 = state_15899__$1;(statearr_15905_15933[1] = 7);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15900 === 5))
{var inst_15883 = (state_15899[2]);var state_15899__$1 = (function (){var statearr_15906 = state_15899;(statearr_15906[9] = inst_15883);
return statearr_15906;
})();return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_15899__$1,12,dchan);
} else
{if((state_val_15900 === 6))
{var state_15899__$1 = state_15899;var statearr_15907_15934 = state_15899__$1;(statearr_15907_15934[2] = null);
(statearr_15907_15934[1] = 11);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15900 === 7))
{var state_15899__$1 = state_15899;var statearr_15908_15935 = state_15899__$1;(statearr_15908_15935[2] = null);
(statearr_15908_15935[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15900 === 8))
{var inst_15881 = (state_15899[2]);var state_15899__$1 = state_15899;var statearr_15909_15936 = state_15899__$1;(statearr_15909_15936[2] = inst_15881);
(statearr_15909_15936[1] = 5);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15900 === 9))
{var inst_15863 = (state_15899[7]);var inst_15876 = (state_15899[2]);var inst_15877 = (inst_15863 + 1);var inst_15863__$1 = inst_15877;var state_15899__$1 = (function (){var statearr_15910 = state_15899;(statearr_15910[7] = inst_15863__$1);
(statearr_15910[10] = inst_15876);
return statearr_15910;
})();var statearr_15911_15937 = state_15899__$1;(statearr_15911_15937[2] = null);
(statearr_15911_15937[1] = 4);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15900 === 10))
{var inst_15867 = (state_15899[2]);var inst_15868 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);var state_15899__$1 = (function (){var statearr_15912 = state_15899;(statearr_15912[11] = inst_15867);
return statearr_15912;
})();var statearr_15913_15938 = state_15899__$1;(statearr_15913_15938[2] = inst_15868);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_15899__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15900 === 11))
{var inst_15863 = (state_15899[7]);var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_15899,10,Object,null,9);var inst_15872 = chs__$1.call(null,inst_15863);var inst_15873 = done.call(null,inst_15863);var inst_15874 = cljs.core.async.take_BANG_.call(null,inst_15872,inst_15873);var state_15899__$1 = state_15899;var statearr_15914_15939 = state_15899__$1;(statearr_15914_15939[2] = inst_15874);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_15899__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15900 === 12))
{var inst_15885 = (state_15899[12]);var inst_15885__$1 = (state_15899[2]);var inst_15886 = cljs.core.some.call(null,cljs.core.nil_QMARK_,inst_15885__$1);var state_15899__$1 = (function (){var statearr_15915 = state_15899;(statearr_15915[12] = inst_15885__$1);
return statearr_15915;
})();if(cljs.core.truth_(inst_15886))
{var statearr_15916_15940 = state_15899__$1;(statearr_15916_15940[1] = 13);
} else
{var statearr_15917_15941 = state_15899__$1;(statearr_15917_15941[1] = 14);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15900 === 13))
{var inst_15888 = cljs.core.async.close_BANG_.call(null,out);var state_15899__$1 = state_15899;var statearr_15918_15942 = state_15899__$1;(statearr_15918_15942[2] = inst_15888);
(statearr_15918_15942[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15900 === 14))
{var inst_15885 = (state_15899[12]);var inst_15890 = cljs.core.apply.call(null,f,inst_15885);var state_15899__$1 = state_15899;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_15899__$1,16,out,inst_15890);
} else
{if((state_val_15900 === 15))
{var inst_15895 = (state_15899[2]);var state_15899__$1 = state_15899;var statearr_15919_15943 = state_15899__$1;(statearr_15919_15943[2] = inst_15895);
(statearr_15919_15943[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_15900 === 16))
{var inst_15892 = (state_15899[2]);var state_15899__$1 = (function (){var statearr_15920 = state_15899;(statearr_15920[13] = inst_15892);
return statearr_15920;
})();var statearr_15921_15944 = state_15899__$1;(statearr_15921_15944[2] = null);
(statearr_15921_15944[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_15925 = (new Array(14));(statearr_15925[0] = state_machine__7152__auto__);
(statearr_15925[1] = 1);
return statearr_15925;
});
var state_machine__7152__auto____1 = (function (state_15899){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_15899);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e15926){if((e15926 instanceof Object))
{var ex__7155__auto__ = e15926;var statearr_15927_15945 = state_15899;(statearr_15927_15945[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_15899);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e15926;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__15946 = state_15899;
state_15899 = G__15946;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_15899){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_15899);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_15928 = f__7222__auto__.call(null);(statearr_15928[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto___15929);
return statearr_15928;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return out;
});
map = function(f,chs,buf_or_n){
switch(arguments.length){
case 2:
return map__2.call(this,f,chs);
case 3:
return map__3.call(this,f,chs,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
map.cljs$core$IFn$_invoke$arity$2 = map__2;
map.cljs$core$IFn$_invoke$arity$3 = map__3;
return map;
})()
;
/**
* Takes a collection of source channels and returns a channel which
* contains all values taken from them. The returned channel will be
* unbuffered by default, or a buf-or-n can be supplied. The channel
* will close after all the source channels have closed.
*/
cljs.core.async.merge = (function() {
var merge = null;
var merge__1 = (function (chs){return merge.call(null,chs,null);
});
var merge__2 = (function (chs,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__7221__auto___16054 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_16030){var state_val_16031 = (state_16030[1]);if((state_val_16031 === 1))
{var inst_16001 = cljs.core.vec.call(null,chs);var inst_16002 = inst_16001;var state_16030__$1 = (function (){var statearr_16032 = state_16030;(statearr_16032[7] = inst_16002);
return statearr_16032;
})();var statearr_16033_16055 = state_16030__$1;(statearr_16033_16055[2] = null);
(statearr_16033_16055[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16031 === 2))
{var inst_16002 = (state_16030[7]);var inst_16004 = cljs.core.count.call(null,inst_16002);var inst_16005 = (inst_16004 > 0);var state_16030__$1 = state_16030;if(cljs.core.truth_(inst_16005))
{var statearr_16034_16056 = state_16030__$1;(statearr_16034_16056[1] = 4);
} else
{var statearr_16035_16057 = state_16030__$1;(statearr_16035_16057[1] = 5);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16031 === 3))
{var inst_16028 = (state_16030[2]);var state_16030__$1 = state_16030;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_16030__$1,inst_16028);
} else
{if((state_val_16031 === 4))
{var inst_16002 = (state_16030[7]);var state_16030__$1 = state_16030;return cljs.core.async.impl.ioc_helpers.ioc_alts_BANG_.call(null,state_16030__$1,7,inst_16002);
} else
{if((state_val_16031 === 5))
{var inst_16024 = cljs.core.async.close_BANG_.call(null,out);var state_16030__$1 = state_16030;var statearr_16036_16058 = state_16030__$1;(statearr_16036_16058[2] = inst_16024);
(statearr_16036_16058[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16031 === 6))
{var inst_16026 = (state_16030[2]);var state_16030__$1 = state_16030;var statearr_16037_16059 = state_16030__$1;(statearr_16037_16059[2] = inst_16026);
(statearr_16037_16059[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16031 === 7))
{var inst_16010 = (state_16030[8]);var inst_16009 = (state_16030[9]);var inst_16009__$1 = (state_16030[2]);var inst_16010__$1 = cljs.core.nth.call(null,inst_16009__$1,0,null);var inst_16011 = cljs.core.nth.call(null,inst_16009__$1,1,null);var inst_16012 = (inst_16010__$1 == null);var state_16030__$1 = (function (){var statearr_16038 = state_16030;(statearr_16038[8] = inst_16010__$1);
(statearr_16038[9] = inst_16009__$1);
(statearr_16038[10] = inst_16011);
return statearr_16038;
})();if(cljs.core.truth_(inst_16012))
{var statearr_16039_16060 = state_16030__$1;(statearr_16039_16060[1] = 8);
} else
{var statearr_16040_16061 = state_16030__$1;(statearr_16040_16061[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16031 === 8))
{var inst_16002 = (state_16030[7]);var inst_16010 = (state_16030[8]);var inst_16009 = (state_16030[9]);var inst_16011 = (state_16030[10]);var inst_16014 = (function (){var c = inst_16011;var v = inst_16010;var vec__16007 = inst_16009;var cs = inst_16002;return ((function (c,v,vec__16007,cs,inst_16002,inst_16010,inst_16009,inst_16011,state_val_16031){
return (function (p1__15947_SHARP_){return cljs.core.not_EQ_.call(null,c,p1__15947_SHARP_);
});
;})(c,v,vec__16007,cs,inst_16002,inst_16010,inst_16009,inst_16011,state_val_16031))
})();var inst_16015 = cljs.core.filterv.call(null,inst_16014,inst_16002);var inst_16002__$1 = inst_16015;var state_16030__$1 = (function (){var statearr_16041 = state_16030;(statearr_16041[7] = inst_16002__$1);
return statearr_16041;
})();var statearr_16042_16062 = state_16030__$1;(statearr_16042_16062[2] = null);
(statearr_16042_16062[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16031 === 9))
{var inst_16010 = (state_16030[8]);var state_16030__$1 = state_16030;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_16030__$1,11,out,inst_16010);
} else
{if((state_val_16031 === 10))
{var inst_16022 = (state_16030[2]);var state_16030__$1 = state_16030;var statearr_16044_16063 = state_16030__$1;(statearr_16044_16063[2] = inst_16022);
(statearr_16044_16063[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16031 === 11))
{var inst_16002 = (state_16030[7]);var inst_16019 = (state_16030[2]);var tmp16043 = inst_16002;var inst_16002__$1 = tmp16043;var state_16030__$1 = (function (){var statearr_16045 = state_16030;(statearr_16045[7] = inst_16002__$1);
(statearr_16045[11] = inst_16019);
return statearr_16045;
})();var statearr_16046_16064 = state_16030__$1;(statearr_16046_16064[2] = null);
(statearr_16046_16064[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_16050 = (new Array(12));(statearr_16050[0] = state_machine__7152__auto__);
(statearr_16050[1] = 1);
return statearr_16050;
});
var state_machine__7152__auto____1 = (function (state_16030){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_16030);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e16051){if((e16051 instanceof Object))
{var ex__7155__auto__ = e16051;var statearr_16052_16065 = state_16030;(statearr_16052_16065[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_16030);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e16051;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__16066 = state_16030;
state_16030 = G__16066;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_16030){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_16030);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_16053 = f__7222__auto__.call(null);(statearr_16053[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto___16054);
return statearr_16053;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return out;
});
merge = function(chs,buf_or_n){
switch(arguments.length){
case 1:
return merge__1.call(this,chs);
case 2:
return merge__2.call(this,chs,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
merge.cljs$core$IFn$_invoke$arity$1 = merge__1;
merge.cljs$core$IFn$_invoke$arity$2 = merge__2;
return merge;
})()
;
/**
* Returns a channel containing the single (collection) result of the
* items taken from the channel conjoined to the supplied
* collection. ch must close before into produces a result.
*/
cljs.core.async.into = (function into(coll,ch){return cljs.core.async.reduce.call(null,cljs.core.conj,coll,ch);
});
/**
* Returns a channel that will return, at most, n items from ch. After n items
* have been returned, or ch has been closed, the return chanel will close.
* 
* The output channel is unbuffered by default, unless buf-or-n is given.
*/
cljs.core.async.take = (function() {
var take = null;
var take__2 = (function (n,ch){return take.call(null,n,ch,null);
});
var take__3 = (function (n,ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__7221__auto___16159 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_16136){var state_val_16137 = (state_16136[1]);if((state_val_16137 === 1))
{var inst_16113 = 0;var state_16136__$1 = (function (){var statearr_16138 = state_16136;(statearr_16138[7] = inst_16113);
return statearr_16138;
})();var statearr_16139_16160 = state_16136__$1;(statearr_16139_16160[2] = null);
(statearr_16139_16160[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16137 === 2))
{var inst_16113 = (state_16136[7]);var inst_16115 = (inst_16113 < n);var state_16136__$1 = state_16136;if(cljs.core.truth_(inst_16115))
{var statearr_16140_16161 = state_16136__$1;(statearr_16140_16161[1] = 4);
} else
{var statearr_16141_16162 = state_16136__$1;(statearr_16141_16162[1] = 5);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16137 === 3))
{var inst_16133 = (state_16136[2]);var inst_16134 = cljs.core.async.close_BANG_.call(null,out);var state_16136__$1 = (function (){var statearr_16142 = state_16136;(statearr_16142[8] = inst_16133);
return statearr_16142;
})();return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_16136__$1,inst_16134);
} else
{if((state_val_16137 === 4))
{var state_16136__$1 = state_16136;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_16136__$1,7,ch);
} else
{if((state_val_16137 === 5))
{var state_16136__$1 = state_16136;var statearr_16143_16163 = state_16136__$1;(statearr_16143_16163[2] = null);
(statearr_16143_16163[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16137 === 6))
{var inst_16131 = (state_16136[2]);var state_16136__$1 = state_16136;var statearr_16144_16164 = state_16136__$1;(statearr_16144_16164[2] = inst_16131);
(statearr_16144_16164[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16137 === 7))
{var inst_16118 = (state_16136[9]);var inst_16118__$1 = (state_16136[2]);var inst_16119 = (inst_16118__$1 == null);var inst_16120 = cljs.core.not.call(null,inst_16119);var state_16136__$1 = (function (){var statearr_16145 = state_16136;(statearr_16145[9] = inst_16118__$1);
return statearr_16145;
})();if(inst_16120)
{var statearr_16146_16165 = state_16136__$1;(statearr_16146_16165[1] = 8);
} else
{var statearr_16147_16166 = state_16136__$1;(statearr_16147_16166[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16137 === 8))
{var inst_16118 = (state_16136[9]);var state_16136__$1 = state_16136;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_16136__$1,11,out,inst_16118);
} else
{if((state_val_16137 === 9))
{var state_16136__$1 = state_16136;var statearr_16148_16167 = state_16136__$1;(statearr_16148_16167[2] = null);
(statearr_16148_16167[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16137 === 10))
{var inst_16128 = (state_16136[2]);var state_16136__$1 = state_16136;var statearr_16149_16168 = state_16136__$1;(statearr_16149_16168[2] = inst_16128);
(statearr_16149_16168[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16137 === 11))
{var inst_16113 = (state_16136[7]);var inst_16123 = (state_16136[2]);var inst_16124 = (inst_16113 + 1);var inst_16113__$1 = inst_16124;var state_16136__$1 = (function (){var statearr_16150 = state_16136;(statearr_16150[7] = inst_16113__$1);
(statearr_16150[10] = inst_16123);
return statearr_16150;
})();var statearr_16151_16169 = state_16136__$1;(statearr_16151_16169[2] = null);
(statearr_16151_16169[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_16155 = (new Array(11));(statearr_16155[0] = state_machine__7152__auto__);
(statearr_16155[1] = 1);
return statearr_16155;
});
var state_machine__7152__auto____1 = (function (state_16136){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_16136);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e16156){if((e16156 instanceof Object))
{var ex__7155__auto__ = e16156;var statearr_16157_16170 = state_16136;(statearr_16157_16170[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_16136);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e16156;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__16171 = state_16136;
state_16136 = G__16171;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_16136){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_16136);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_16158 = f__7222__auto__.call(null);(statearr_16158[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto___16159);
return statearr_16158;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return out;
});
take = function(n,ch,buf_or_n){
switch(arguments.length){
case 2:
return take__2.call(this,n,ch);
case 3:
return take__3.call(this,n,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
take.cljs$core$IFn$_invoke$arity$2 = take__2;
take.cljs$core$IFn$_invoke$arity$3 = take__3;
return take;
})()
;
/**
* Returns a channel that will contain values from ch. Consecutive duplicate
* values will be dropped.
* 
* The output channel is unbuffered by default, unless buf-or-n is given.
*/
cljs.core.async.unique = (function() {
var unique = null;
var unique__1 = (function (ch){return unique.call(null,ch,null);
});
var unique__2 = (function (ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__7221__auto___16268 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_16243){var state_val_16244 = (state_16243[1]);if((state_val_16244 === 1))
{var inst_16220 = null;var state_16243__$1 = (function (){var statearr_16245 = state_16243;(statearr_16245[7] = inst_16220);
return statearr_16245;
})();var statearr_16246_16269 = state_16243__$1;(statearr_16246_16269[2] = null);
(statearr_16246_16269[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16244 === 2))
{var state_16243__$1 = state_16243;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_16243__$1,4,ch);
} else
{if((state_val_16244 === 3))
{var inst_16240 = (state_16243[2]);var inst_16241 = cljs.core.async.close_BANG_.call(null,out);var state_16243__$1 = (function (){var statearr_16247 = state_16243;(statearr_16247[8] = inst_16240);
return statearr_16247;
})();return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_16243__$1,inst_16241);
} else
{if((state_val_16244 === 4))
{var inst_16223 = (state_16243[9]);var inst_16223__$1 = (state_16243[2]);var inst_16224 = (inst_16223__$1 == null);var inst_16225 = cljs.core.not.call(null,inst_16224);var state_16243__$1 = (function (){var statearr_16248 = state_16243;(statearr_16248[9] = inst_16223__$1);
return statearr_16248;
})();if(inst_16225)
{var statearr_16249_16270 = state_16243__$1;(statearr_16249_16270[1] = 5);
} else
{var statearr_16250_16271 = state_16243__$1;(statearr_16250_16271[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16244 === 5))
{var inst_16223 = (state_16243[9]);var inst_16220 = (state_16243[7]);var inst_16227 = cljs.core._EQ_.call(null,inst_16223,inst_16220);var state_16243__$1 = state_16243;if(inst_16227)
{var statearr_16251_16272 = state_16243__$1;(statearr_16251_16272[1] = 8);
} else
{var statearr_16252_16273 = state_16243__$1;(statearr_16252_16273[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16244 === 6))
{var state_16243__$1 = state_16243;var statearr_16254_16274 = state_16243__$1;(statearr_16254_16274[2] = null);
(statearr_16254_16274[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16244 === 7))
{var inst_16238 = (state_16243[2]);var state_16243__$1 = state_16243;var statearr_16255_16275 = state_16243__$1;(statearr_16255_16275[2] = inst_16238);
(statearr_16255_16275[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16244 === 8))
{var inst_16220 = (state_16243[7]);var tmp16253 = inst_16220;var inst_16220__$1 = tmp16253;var state_16243__$1 = (function (){var statearr_16256 = state_16243;(statearr_16256[7] = inst_16220__$1);
return statearr_16256;
})();var statearr_16257_16276 = state_16243__$1;(statearr_16257_16276[2] = null);
(statearr_16257_16276[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16244 === 9))
{var inst_16223 = (state_16243[9]);var state_16243__$1 = state_16243;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_16243__$1,11,out,inst_16223);
} else
{if((state_val_16244 === 10))
{var inst_16235 = (state_16243[2]);var state_16243__$1 = state_16243;var statearr_16258_16277 = state_16243__$1;(statearr_16258_16277[2] = inst_16235);
(statearr_16258_16277[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16244 === 11))
{var inst_16223 = (state_16243[9]);var inst_16232 = (state_16243[2]);var inst_16220 = inst_16223;var state_16243__$1 = (function (){var statearr_16259 = state_16243;(statearr_16259[7] = inst_16220);
(statearr_16259[10] = inst_16232);
return statearr_16259;
})();var statearr_16260_16278 = state_16243__$1;(statearr_16260_16278[2] = null);
(statearr_16260_16278[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_16264 = (new Array(11));(statearr_16264[0] = state_machine__7152__auto__);
(statearr_16264[1] = 1);
return statearr_16264;
});
var state_machine__7152__auto____1 = (function (state_16243){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_16243);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e16265){if((e16265 instanceof Object))
{var ex__7155__auto__ = e16265;var statearr_16266_16279 = state_16243;(statearr_16266_16279[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_16243);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e16265;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__16280 = state_16243;
state_16243 = G__16280;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_16243){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_16243);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_16267 = f__7222__auto__.call(null);(statearr_16267[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto___16268);
return statearr_16267;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return out;
});
unique = function(ch,buf_or_n){
switch(arguments.length){
case 1:
return unique__1.call(this,ch);
case 2:
return unique__2.call(this,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
unique.cljs$core$IFn$_invoke$arity$1 = unique__1;
unique.cljs$core$IFn$_invoke$arity$2 = unique__2;
return unique;
})()
;
/**
* Returns a channel that will contain vectors of n items taken from ch. The
* final vector in the return channel may be smaller than n if ch closed before
* the vector could be completely filled.
* 
* The output channel is unbuffered by default, unless buf-or-n is given
*/
cljs.core.async.partition = (function() {
var partition = null;
var partition__2 = (function (n,ch){return partition.call(null,n,ch,null);
});
var partition__3 = (function (n,ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__7221__auto___16415 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_16385){var state_val_16386 = (state_16385[1]);if((state_val_16386 === 1))
{var inst_16348 = (new Array(n));var inst_16349 = inst_16348;var inst_16350 = 0;var state_16385__$1 = (function (){var statearr_16387 = state_16385;(statearr_16387[7] = inst_16349);
(statearr_16387[8] = inst_16350);
return statearr_16387;
})();var statearr_16388_16416 = state_16385__$1;(statearr_16388_16416[2] = null);
(statearr_16388_16416[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16386 === 2))
{var state_16385__$1 = state_16385;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_16385__$1,4,ch);
} else
{if((state_val_16386 === 3))
{var inst_16383 = (state_16385[2]);var state_16385__$1 = state_16385;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_16385__$1,inst_16383);
} else
{if((state_val_16386 === 4))
{var inst_16353 = (state_16385[9]);var inst_16353__$1 = (state_16385[2]);var inst_16354 = (inst_16353__$1 == null);var inst_16355 = cljs.core.not.call(null,inst_16354);var state_16385__$1 = (function (){var statearr_16389 = state_16385;(statearr_16389[9] = inst_16353__$1);
return statearr_16389;
})();if(inst_16355)
{var statearr_16390_16417 = state_16385__$1;(statearr_16390_16417[1] = 5);
} else
{var statearr_16391_16418 = state_16385__$1;(statearr_16391_16418[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16386 === 5))
{var inst_16358 = (state_16385[10]);var inst_16349 = (state_16385[7]);var inst_16350 = (state_16385[8]);var inst_16353 = (state_16385[9]);var inst_16357 = (inst_16349[inst_16350] = inst_16353);var inst_16358__$1 = (inst_16350 + 1);var inst_16359 = (inst_16358__$1 < n);var state_16385__$1 = (function (){var statearr_16392 = state_16385;(statearr_16392[10] = inst_16358__$1);
(statearr_16392[11] = inst_16357);
return statearr_16392;
})();if(cljs.core.truth_(inst_16359))
{var statearr_16393_16419 = state_16385__$1;(statearr_16393_16419[1] = 8);
} else
{var statearr_16394_16420 = state_16385__$1;(statearr_16394_16420[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16386 === 6))
{var inst_16350 = (state_16385[8]);var inst_16371 = (inst_16350 > 0);var state_16385__$1 = state_16385;if(cljs.core.truth_(inst_16371))
{var statearr_16396_16421 = state_16385__$1;(statearr_16396_16421[1] = 12);
} else
{var statearr_16397_16422 = state_16385__$1;(statearr_16397_16422[1] = 13);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16386 === 7))
{var inst_16381 = (state_16385[2]);var state_16385__$1 = state_16385;var statearr_16398_16423 = state_16385__$1;(statearr_16398_16423[2] = inst_16381);
(statearr_16398_16423[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16386 === 8))
{var inst_16358 = (state_16385[10]);var inst_16349 = (state_16385[7]);var tmp16395 = inst_16349;var inst_16349__$1 = tmp16395;var inst_16350 = inst_16358;var state_16385__$1 = (function (){var statearr_16399 = state_16385;(statearr_16399[7] = inst_16349__$1);
(statearr_16399[8] = inst_16350);
return statearr_16399;
})();var statearr_16400_16424 = state_16385__$1;(statearr_16400_16424[2] = null);
(statearr_16400_16424[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16386 === 9))
{var inst_16349 = (state_16385[7]);var inst_16363 = cljs.core.vec.call(null,inst_16349);var state_16385__$1 = state_16385;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_16385__$1,11,out,inst_16363);
} else
{if((state_val_16386 === 10))
{var inst_16369 = (state_16385[2]);var state_16385__$1 = state_16385;var statearr_16401_16425 = state_16385__$1;(statearr_16401_16425[2] = inst_16369);
(statearr_16401_16425[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16386 === 11))
{var inst_16365 = (state_16385[2]);var inst_16366 = (new Array(n));var inst_16349 = inst_16366;var inst_16350 = 0;var state_16385__$1 = (function (){var statearr_16402 = state_16385;(statearr_16402[7] = inst_16349);
(statearr_16402[8] = inst_16350);
(statearr_16402[12] = inst_16365);
return statearr_16402;
})();var statearr_16403_16426 = state_16385__$1;(statearr_16403_16426[2] = null);
(statearr_16403_16426[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16386 === 12))
{var inst_16349 = (state_16385[7]);var inst_16373 = cljs.core.vec.call(null,inst_16349);var state_16385__$1 = state_16385;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_16385__$1,15,out,inst_16373);
} else
{if((state_val_16386 === 13))
{var state_16385__$1 = state_16385;var statearr_16404_16427 = state_16385__$1;(statearr_16404_16427[2] = null);
(statearr_16404_16427[1] = 14);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16386 === 14))
{var inst_16378 = (state_16385[2]);var inst_16379 = cljs.core.async.close_BANG_.call(null,out);var state_16385__$1 = (function (){var statearr_16405 = state_16385;(statearr_16405[13] = inst_16378);
return statearr_16405;
})();var statearr_16406_16428 = state_16385__$1;(statearr_16406_16428[2] = inst_16379);
(statearr_16406_16428[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16386 === 15))
{var inst_16375 = (state_16385[2]);var state_16385__$1 = state_16385;var statearr_16407_16429 = state_16385__$1;(statearr_16407_16429[2] = inst_16375);
(statearr_16407_16429[1] = 14);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_16411 = (new Array(14));(statearr_16411[0] = state_machine__7152__auto__);
(statearr_16411[1] = 1);
return statearr_16411;
});
var state_machine__7152__auto____1 = (function (state_16385){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_16385);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e16412){if((e16412 instanceof Object))
{var ex__7155__auto__ = e16412;var statearr_16413_16430 = state_16385;(statearr_16413_16430[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_16385);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e16412;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__16431 = state_16385;
state_16385 = G__16431;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_16385){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_16385);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_16414 = f__7222__auto__.call(null);(statearr_16414[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto___16415);
return statearr_16414;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return out;
});
partition = function(n,ch,buf_or_n){
switch(arguments.length){
case 2:
return partition__2.call(this,n,ch);
case 3:
return partition__3.call(this,n,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
partition.cljs$core$IFn$_invoke$arity$2 = partition__2;
partition.cljs$core$IFn$_invoke$arity$3 = partition__3;
return partition;
})()
;
/**
* Returns a channel that will contain vectors of items taken from ch. New
* vectors will be created whenever (f itm) returns a value that differs from
* the previous item's (f itm).
* 
* The output channel is unbuffered, unless buf-or-n is given
*/
cljs.core.async.partition_by = (function() {
var partition_by = null;
var partition_by__2 = (function (f,ch){return partition_by.call(null,f,ch,null);
});
var partition_by__3 = (function (f,ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__7221__auto___16574 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,(function (){var f__7222__auto__ = (function (){var switch__7151__auto__ = (function (state_16544){var state_val_16545 = (state_16544[1]);if((state_val_16545 === 1))
{var inst_16503 = (new Array(0));var inst_16504 = inst_16503;var inst_16505 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",4382193538);var state_16544__$1 = (function (){var statearr_16546 = state_16544;(statearr_16546[7] = inst_16505);
(statearr_16546[8] = inst_16504);
return statearr_16546;
})();var statearr_16547_16575 = state_16544__$1;(statearr_16547_16575[2] = null);
(statearr_16547_16575[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16545 === 2))
{var state_16544__$1 = state_16544;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_16544__$1,4,ch);
} else
{if((state_val_16545 === 3))
{var inst_16542 = (state_16544[2]);var state_16544__$1 = state_16544;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_16544__$1,inst_16542);
} else
{if((state_val_16545 === 4))
{var inst_16508 = (state_16544[9]);var inst_16508__$1 = (state_16544[2]);var inst_16509 = (inst_16508__$1 == null);var inst_16510 = cljs.core.not.call(null,inst_16509);var state_16544__$1 = (function (){var statearr_16548 = state_16544;(statearr_16548[9] = inst_16508__$1);
return statearr_16548;
})();if(inst_16510)
{var statearr_16549_16576 = state_16544__$1;(statearr_16549_16576[1] = 5);
} else
{var statearr_16550_16577 = state_16544__$1;(statearr_16550_16577[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16545 === 5))
{var inst_16505 = (state_16544[7]);var inst_16512 = (state_16544[10]);var inst_16508 = (state_16544[9]);var inst_16512__$1 = f.call(null,inst_16508);var inst_16513 = cljs.core._EQ_.call(null,inst_16512__$1,inst_16505);var inst_16514 = cljs.core.keyword_identical_QMARK_.call(null,inst_16505,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",4382193538));var inst_16515 = (inst_16513) || (inst_16514);var state_16544__$1 = (function (){var statearr_16551 = state_16544;(statearr_16551[10] = inst_16512__$1);
return statearr_16551;
})();if(cljs.core.truth_(inst_16515))
{var statearr_16552_16578 = state_16544__$1;(statearr_16552_16578[1] = 8);
} else
{var statearr_16553_16579 = state_16544__$1;(statearr_16553_16579[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16545 === 6))
{var inst_16504 = (state_16544[8]);var inst_16529 = inst_16504.length;var inst_16530 = (inst_16529 > 0);var state_16544__$1 = state_16544;if(cljs.core.truth_(inst_16530))
{var statearr_16555_16580 = state_16544__$1;(statearr_16555_16580[1] = 12);
} else
{var statearr_16556_16581 = state_16544__$1;(statearr_16556_16581[1] = 13);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16545 === 7))
{var inst_16540 = (state_16544[2]);var state_16544__$1 = state_16544;var statearr_16557_16582 = state_16544__$1;(statearr_16557_16582[2] = inst_16540);
(statearr_16557_16582[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16545 === 8))
{var inst_16512 = (state_16544[10]);var inst_16508 = (state_16544[9]);var inst_16504 = (state_16544[8]);var inst_16517 = inst_16504.push(inst_16508);var tmp16554 = inst_16504;var inst_16504__$1 = tmp16554;var inst_16505 = inst_16512;var state_16544__$1 = (function (){var statearr_16558 = state_16544;(statearr_16558[7] = inst_16505);
(statearr_16558[8] = inst_16504__$1);
(statearr_16558[11] = inst_16517);
return statearr_16558;
})();var statearr_16559_16583 = state_16544__$1;(statearr_16559_16583[2] = null);
(statearr_16559_16583[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16545 === 9))
{var inst_16504 = (state_16544[8]);var inst_16520 = cljs.core.vec.call(null,inst_16504);var state_16544__$1 = state_16544;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_16544__$1,11,out,inst_16520);
} else
{if((state_val_16545 === 10))
{var inst_16527 = (state_16544[2]);var state_16544__$1 = state_16544;var statearr_16560_16584 = state_16544__$1;(statearr_16560_16584[2] = inst_16527);
(statearr_16560_16584[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16545 === 11))
{var inst_16512 = (state_16544[10]);var inst_16508 = (state_16544[9]);var inst_16522 = (state_16544[2]);var inst_16523 = (new Array(0));var inst_16524 = inst_16523.push(inst_16508);var inst_16504 = inst_16523;var inst_16505 = inst_16512;var state_16544__$1 = (function (){var statearr_16561 = state_16544;(statearr_16561[7] = inst_16505);
(statearr_16561[8] = inst_16504);
(statearr_16561[12] = inst_16522);
(statearr_16561[13] = inst_16524);
return statearr_16561;
})();var statearr_16562_16585 = state_16544__$1;(statearr_16562_16585[2] = null);
(statearr_16562_16585[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16545 === 12))
{var inst_16504 = (state_16544[8]);var inst_16532 = cljs.core.vec.call(null,inst_16504);var state_16544__$1 = state_16544;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_16544__$1,15,out,inst_16532);
} else
{if((state_val_16545 === 13))
{var state_16544__$1 = state_16544;var statearr_16563_16586 = state_16544__$1;(statearr_16563_16586[2] = null);
(statearr_16563_16586[1] = 14);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16545 === 14))
{var inst_16537 = (state_16544[2]);var inst_16538 = cljs.core.async.close_BANG_.call(null,out);var state_16544__$1 = (function (){var statearr_16564 = state_16544;(statearr_16564[14] = inst_16537);
return statearr_16564;
})();var statearr_16565_16587 = state_16544__$1;(statearr_16565_16587[2] = inst_16538);
(statearr_16565_16587[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_16545 === 15))
{var inst_16534 = (state_16544[2]);var state_16544__$1 = state_16544;var statearr_16566_16588 = state_16544__$1;(statearr_16566_16588[2] = inst_16534);
(statearr_16566_16588[1] = 14);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});return ((function (switch__7151__auto__){
return (function() {
var state_machine__7152__auto__ = null;
var state_machine__7152__auto____0 = (function (){var statearr_16570 = (new Array(15));(statearr_16570[0] = state_machine__7152__auto__);
(statearr_16570[1] = 1);
return statearr_16570;
});
var state_machine__7152__auto____1 = (function (state_16544){while(true){
var ret_value__7153__auto__ = (function (){try{while(true){
var result__7154__auto__ = switch__7151__auto__.call(null,state_16544);if(cljs.core.keyword_identical_QMARK_.call(null,result__7154__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7154__auto__;
}
break;
}
}catch (e16571){if((e16571 instanceof Object))
{var ex__7155__auto__ = e16571;var statearr_16572_16589 = state_16544;(statearr_16572_16589[5] = ex__7155__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_16544);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e16571;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7153__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__16590 = state_16544;
state_16544 = G__16590;
continue;
}
} else
{return ret_value__7153__auto__;
}
break;
}
});
state_machine__7152__auto__ = function(state_16544){
switch(arguments.length){
case 0:
return state_machine__7152__auto____0.call(this);
case 1:
return state_machine__7152__auto____1.call(this,state_16544);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7152__auto____0;
state_machine__7152__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7152__auto____1;
return state_machine__7152__auto__;
})()
;})(switch__7151__auto__))
})();var state__7223__auto__ = (function (){var statearr_16573 = f__7222__auto__.call(null);(statearr_16573[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7221__auto___16574);
return statearr_16573;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7223__auto__);
}));
return out;
});
partition_by = function(f,ch,buf_or_n){
switch(arguments.length){
case 2:
return partition_by__2.call(this,f,ch);
case 3:
return partition_by__3.call(this,f,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
partition_by.cljs$core$IFn$_invoke$arity$2 = partition_by__2;
partition_by.cljs$core$IFn$_invoke$arity$3 = partition_by__3;
return partition_by;
})()
;

//# sourceMappingURL=async.js.map